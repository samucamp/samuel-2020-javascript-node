//CONTEXTO LÉXICO
const valor = 'Global'

function minhaFuncao() {
    //como não tem declaração, busca em contexto mais abrangente
    //não utiliza valor 'Local', mas busca em 'Global'
    console.log(valor)
}

function exec() {
    const valor = 'Local'
    minhaFuncao()
}

exec()
//Quando uma função é declarada, ela carrega consigo
//qual contexto ela foi escrita.


//CONCEITO DE CLOSURES

//Closure é o escopo criado quando uma função é declarada
//Esse escopo permite a função acessar e manipular variáveis
//externas à função

//Contexto léxico em ação!

const x = 'Global'

function fora() {
    const x = 'Local'

    function dentro() {
        return x
    }
    return dentro
}

const minhaFuncao2 = fora()
console.log(minhaFuncao2())