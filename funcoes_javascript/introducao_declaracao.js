// CIDADÃO DE PRIMEIRA CLASSE
// FIRST CLASS OBJECT (Citizens)
// HIGHER ORDER FUNCTION
// Pode ser tratado com dado em JS

/* criando função de forma literal */

//retorna undefined senão utilizar return
function fun1() {} //bloco obrigatório em funções

/* pode armazenar função em variável */
//colocou uma função anônima em uma variável
const fun2 = function () {}

/* declarar e armazenar função em array */
const array = [function (a, b) {
    return a + b
}, fun1, fun2]
console.log(array[0](2, 3))

/* Armazenar em um atributo de objeto */
const obj = {}
obj.falar = function () {
    return 'Opa'
}
console.log(obj.falar())

/* Passar função como parâmetro */
function run(fun) {
    fun()
}

run(function () {
    console.log('Executando...')
})

/* uma função pode retornar/conter outra função */

function soma(a, b) {
    return function (c) {
        console.log(a + b + c)
    }
}
soma(2, 3)(4)
//outra forma
const cincoMais = soma(2, 3)
cincoMais(4)

console.log(SOMA(3, 4))

//FUNCTION DECLARATION
//Essa função é interpretada previamente
//pode acessar essa função em linha anterior
function SOMA(x, y) {
    return x + y
}

//FUNCTION EXPRESSION
const sub = function (x, y) {
    return x - y
}

console.log(sub(3, 4))

//NAMED FUNCTION EXPRESSION
//vantagem em debugar, aparece nome da função em diagnostico
const mult = function mult(x, y) {
    return x * y
}

console.log(mult(3, 4))


//CONCEITO DE IIFE
// IIFE = Immediately Invoked Function Expression

//para fugir do escopo global
/*
(function() {
    console.log('Será executado na hora!')
    console.log('Foge do escopo mais abrangente')
})()
*/


//CONCEITO CALL E APPLY 
//(FORMAS DIFERENTES DE CHAMAR E EXECUTAR FUNÇÃO)
function getPreco(imposto = 0, moeda = 'R$') {
    return `${moeda} ${this.preco * (1-this.desc)*(1+imposto)}`
}

const produto = {
    nome: 'Notebook',
    preco: 4589,
    desc: 0.15,
    getPreco
}

//UTILIZANDO O OBJETO COM A FUNÇÃO
global.preco = 20
global.desc = 0.1
console.log(getPreco()) //pega do global
console.log(produto.getPreco()) //pega do objeto e função

//UTILIZANDO CALL E APPLY
const carro = {
    preco: 49990,
    desc: 0.20
}
console.log(getPreco.call(carro))
console.log(getPreco.apply(carro))
//diferença: passagem de parâmetros
console.log(getPreco.call(carro, 0.17, '$'))
//tipo call: passa diretamente cada parâmetros
//1o = contexto
//depois = parâmetros pra função
console.log(getPreco.apply(carro, [0.17, '$']))
//1o contexto
//array com parâmetros