/********* AULA 1 **********/

let dobro = function (a) {
    return 2 * a
}

dobro = (a) => {
    return 2 * a
}

dobro = a => 2 * a //return implicito

console.log(dobro(Math.PI))

let ola = function () {
    return 'Olá'
}

ola = () => 'Olá'
ola = _ => 'Olá' //único parâmetro, pode ser ignorado

console.log(ola())


/********* AULA 2 **********/

//com arrow function não precisa usar o bind
/*
function Pessoa(){
    this.idade = 0

    setInterval( () => {
        this.idade++
        console.log(this.idade)
    }, 1000)
}

new Pessoa
*/

/********* AULA 3 **********/

let comparaComThis = function (param) {
    console.log(this === param)
}

comparaComThis(global)
comparaComThis(this)

const obj = {}
comparaComThis = comparaComThis.bind(obj)
comparaComThis(global)
comparaComThis(obj)

let comparaComThisArrow = param => console.log(this === param)
comparaComThisArrow(global)
comparaComThisArrow(module.exports)
comparaComThisArrow(this)

comparaComThisArrow = comparaComThisArrow.bind(obj) //não consegue
//arrow function é mais forte que o bind
comparaComThisArrow(obj)
comparaComThisArrow(module.exports)