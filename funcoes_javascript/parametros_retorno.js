//PARÂMETROS E RETORNO SÃO OPCIONAIS
function area(largura, altura) {
    const area = largura * altura
    if (area > 20) {
        console.log(`Valor acima do permitido: ${area}m2.`)
    } else {
        return area
    }
}

console.log(area(10, 1.2))
console.log(area(10, 3))
// retorna undefined também, pois não executou return


/************************************/
//PARÂMETROS VARIÁVEIS
function soma() {
    let soma = 0
    //arguments = recupera parâmetros
    //arguments = array interno com todos argumentos passados
    //apesar da function não ter parâmetros
    for (i in arguments) {
        soma += arguments[i]
    }
    return soma
}

console.log(soma())
console.log(soma(1))
console.log(soma(1.1, 2.2, 3.1))
console.log(soma(1.1, 2.2, "Teste"))
console.log(soma('a', 'b', 'c'))

/************************************/
//PARÂMETROS PADRÃO, A PROVA DE ERROS

//Essa versão pode dar problemas com valores 0
function soma1(a, b, c) {
    a = a || 1
    //a = a || 'texto' também funciona
    b = b || 1
    c = c || 1
    return a + b + c
}

console.log(soma1(), soma1(3), soma1(1, 2, 3), soma1(0, 0, 0))

function soma2(a, b, c) {
    a = a !== undefined ? a : 1 //operador ternário
    b = 1 in arguments ? b : 1
    c = isNaN(c) ? 1 : c
    return a + b + c
}
console.log('Novo teste')
console.log(soma2())
console.log(soma2(3))
console.log(soma(1, 2, 3))
console.log(soma(0, 0, 0))

//VALOR PADRÃO ATUALIZAÇÃO ES2015
function soma3(a = 1, b = 1, c = 1) {
    return a + b + c
}
console.log('Teste com ES2015')
console.log(soma3(), soma3(3), soma3(1, 2, 3), soma3(0, 0, 0))