//FUNÇÃO QUE RETORNA UM OBJETO

const prod1 = {
    nome: 'Produto',
    preco: 45
}

//Para gerar vários objetos do mesmo tipo
//Função factory, p retornar novos objetos
//Parecido com idéia de classes

function criarPessoa() {
    return {
        nome: 'Ana',
        sobrenome: 'Silva'
    }
}

console.log(criarPessoa())

//CRIAR PRODUTO EXERCÍCIO

function criarProduto(nome, preco) {
    return {
        nome, //como foi recebido como parâmetro, n precisa
        preco, //como foi recebido como parâmetro, n precisa
        desconto: 0.1
    }
}

console.log(criarProduto('Notebook', 2199.49))
console.log(criarProduto('iPad', 1199.49))


// CLASSE VS FUNCTION
//CLASSE
class Pessoa {
    constructor(nome) {
        this.nome = nome
    }

    falar() {
        console.log(`Meu nome é ${this.nome}`)
    }
}

const p1 = new Pessoa('João')
p1.falar()

//FUNCTION ARROW
//VANTAGEM POIS NÃO PRECISA USAR THIS
const criarpessoa = nome => {
    return {
        falar: () => console.log(`Meu nome é ${nome}`)
    }
}

const p2 = criarpessoa('João')
p2.falar()

//TRANSFORMANDO A CLASSE EM FUNÇÃO CONSTRUTORA
//A CLASSE É UMA FUNÇÃO CONSTRUTORA, COM SINTAXE DIFERENTE
function Pessoa2(nome) {
    this.nome = nome
    this.falar = function () {
        console.log(`Meu nome é ${this.nome}`)
    }
}

const p3 = new Pessoa2('João')
p3.falar()

//TUDO É FUNÇÃO
console.log(typeof Pessoa)
console.log(typeof Pessoa2)
console.log(typeof criarpessoa)