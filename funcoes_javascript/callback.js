// CALLBACK 1

const fabricantes = ["Mercedes", "Audi", "BMW"]

function imprimir(nome, indice) {
    console.log(`${indice+1}. ${nome}`)
}
//forEach é proprio do JS
//FUNÇÃO CALL BACK
//pra cada elemento, chama função de volta (callback)
fabricantes.forEach(imprimir)
fabricantes.forEach(function (a) {
    console.log(a)
})

// CALLBACK 2

const notas = [7.7, 6.5, 4.2, 8.9, 3.6, 7.1, 9.0]

//Sem callback

let notasBaixas = []
for (let i in notas) {
    if (notas[i] < 7) {
        notasBaixas.push(notas[i])
    }
}

console.log(notasBaixas)

//Com callback
//não altera array notas
//altera notasBaixas, removendo os resultados 'falsos'
//filtra o que for 'verdadeiro'
notasBaixas = notas.filter(function (nota) {
    return nota < 7
})

console.log(notasBaixas)

//outra opção com arrow
const notasBaixas3 = notas.filter(nota => nota < 7)
console.log(notasBaixas3)