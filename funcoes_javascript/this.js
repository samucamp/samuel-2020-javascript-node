//Classe define os atributos de cada objeto
//this é um objeto, e acessa o dono do contexto executado no momento

//no browser, this aponta para window iniicialmente
//em java script, this pode variar
//this varia conforme a função foi chamada

// FUNÇÃO ARROW
// 1o motivo = sintaxe reduzida
// 2o motivo = this não varia

/*** THIS E BIND ***/
const pessoa = {
    saudacao: 'Bom dia',
    //es2015 recurso para declarar função direto
    falar() {
        console.log(this.saudacao)
    }
}

pessoa.falar()
//contexto pessoa, this ok.

const falar = pessoa.falar
falar() //contexto mudou, this undefined

const falarDePessoa = pessoa.falar.bind(pessoa) //bind this pessoa
falarDePessoa()

function Pessoa() {
    this.idade = 0

    const self = this
    //setInterval dispara uma função a partir de um intervalo
    setInterval(function () {
        this.idade++ //contexto != de Pessoa
        //precisa de usar .bind
        //console.log(this.idade) - OUTRA OPÇÃO
        console.log(self.idade) // Drible do this
    } /*.bind(this)*/ , 1000) //intervalo em milissegundos
}

new Pessoa