const path = require('path');

const rootDir = require('../utils/path');

const express = require('express');
const router = express.Router();

const adminData = require('./admin');

// __dirname points to routes folder
// go back with '../'
router.get('/', (req, res, next) => {
    //res.send('<h1>Hellow world </h1>');

    console.log('shop.js', adminData.products);

    res.sendFile(path.join(rootDir, 'views', 'shop.html'));
});

module.exports = router;