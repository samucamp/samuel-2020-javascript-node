const path = require('path');

const express = require('express');

const rootDir = require('../utils/path');

const router = express.Router();

const products = [];

// /admin/add-product => GET
router.get('/add-product', (req, res, next) => {
    // res.send('<h1>Add product page</h1><form action="/admin/add-product" method="POST"><input type="text" name="title"><button type="submit">Send</button></form>');

    // res.sendFile(path.join(__dirname, '..', 'views', 'add-product.html'));

    res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
});

// /admin/add-product => POST
router.post('/add-product', (req, res, next) => {
    console.log('admin.js', req.body);

    products.push({
        title: req.body.title
    });

    res.redirect('/');
});

// module.exports = router;

exports.routes = router;
exports.products = products;