const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const adminData = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const app = express();

// parsing
app.use(bodyParser.urlencoded({
    exended: false
}));

// serving public css
// express will forward to public folder
// html cannot link to /public/css
// html has to link to /css
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminData.routes);
app.use(shopRoutes);

// in case wrong /
app.use((req, res, next) => {
    res.status(404);
    // res.send('<h1>Page not found</h1>')
    res.sendFile(path.join(__dirname, 'views', '404.html'));
});


app.listen(3000);