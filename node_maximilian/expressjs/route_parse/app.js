// core modules
// const http = require('http');

// 3rd party package
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

//parsing
app.use(bodyParser.urlencoded({
    exended: false
}));

// by default is '/'
// use method = permite adicionar middlewares
app.use((req, res, next) => {
    // funcao sera executada para todos requests
    console.log('Im in the middleware ' + 'this will always run');
    next();
    // permite request seguir para proximo middleware
});

//'/add-product' deve vir antes de '/', para ser executado primeiro e não utilizar 'next()'
app.use('/add-product', (req, res, next) => {
    console.log('Im in add-product page');
    res.send('<h1>Add product page</h1><form action="/product" method="POST"><input type="text" name="title"><button type="submit">Send</button>');
});

// filter with POST, change .use to .post
app.post('/product', (req, res, next) => {
    console.log('this will only run with post request')
    console.log(req.body)
    res.redirect('/add-product')
});

// by default is '/'
// handle starting with '/', '/products' also works
app.use('/', (req, res, next) => {
    // funcao sera executada para todos requests
    console.log('Im in home');
    res.send('<h1>Hellow world </h1>');
    // enviar response
    // content-type is set to text/html by default
    // não precisa de setHeader
});

// const server = http.createServer(app);
// server.listen(3000);

app.listen(3000);