const express = require('express');
const bodyParser = require('body-parser')

const feedRoutes = require('./routes/feed')

const app = express();

app.use(bodyParser.json());


// CORS (when requesting from outside)
// request from html page not served by this server
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    // * can be changed to specific domain
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})


app.use('/feed', feedRoutes)

app.listen(5000, '127.0.0.1', () => {
    console.log('Server started')
}); 