# MVC

Separation of concerns

## Models

Represents your data in your code

Responsible for managin data (saving, fetching)

Contains data-related logic

## Views

What the users sees

Decoupled from your application code

Shouldn't contain too much logic (ex: handlebars)

## Controllers

    Routes

    Split across middleware Functions

Connecting your models and your views

Responsible to make sure thant model and view can communicate (in both directions)

Contains the "in-between" logic
