// NATIVE MODULES
const path = require('path');

// THIRD PARTY MODULES
const express = require('express');
const bodyParser = require('body-parser');

// OWN MODULES
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errorController = require('./controllers/error')

// Initializing ejs engine with express
const app = express();
app.set('view engine', 'ejs');
app.set('views', 'views'); // pointing to views

// parsing easy w/ bodyParser
app.use(bodyParser.urlencoded({
  extended: false
}));

// pointing static path to 'public'
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(errorController.get404);

app.listen(3000);