//const products = [];

const fs = require('fs');
const path = require('path');

const p = path.join(path.dirname(process.mainModule.filename), 'data', 'products.json');

// this callback 'cb' is necessary to inform the caller of the function when its done
const getProductsFromFile = cb => {
    //return products;
    fs.readFile(p, (err, fileContent) => {
        if (err) {
            //return [];
            cb([]);
        } else {
            //return JSON.parse(fileContent);
            cb(JSON.parse(fileContent));
        }
    });
}

module.exports = class Product {
    constructor(title) {
        this.title = title;

    };

    save() {
        //products.push(this);
        // const p = path.join(path.dirname(process.mainModule.filename), 'data', 'products.json');
        getProductsFromFile(products => {
            products.push(this);
            fs.writeFile(p, JSON.stringify(products), (err) => {
                console.log(err);
            });
        });

        // fs.readFile(p, (err, fileContent) => {
        //     let products = [];
        //     if (!err) {
        //         products = JSON.parse(fileContent);
        //     }
        //     products.push(this);

        // });
    };

    static fetchAll(cb) {
        getProductsFromFile(cb);
    };
};