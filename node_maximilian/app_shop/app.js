// core modules
const path = require('path');

// third party modules
const express = require('express');
const bodyParser = require('body-parser');

// definitions
const PORT = 3000;
const HOST = 'localhost';

// app imports
const errorControllers = require('./controllers/error');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

// express -> app
const app = express();

// template engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// body parser forms urlencoded
app.use(bodyParser.urlencoded({
    extended: false //just strings
}));

// serving static files
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use('/admin', adminRoutes);
app.use(shopRoutes);

// middleware for error 404
app.use(errorControllers.get404);

// bind to HOST:PORT
app.listen(PORT, HOST, () => {
    console.log(`Server is listening to ${HOST}:${PORT}`);
});