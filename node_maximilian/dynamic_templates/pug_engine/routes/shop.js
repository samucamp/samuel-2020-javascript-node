const path = require('path');

const rootDir = require('../utils/path');

const express = require('express');
const router = express.Router();

const adminData = require('./admin');

router.get('/', (req, res, next) => {
    console.log('shop.js', adminData.products);

    // res.sendFile(path.join(rootDir, 'views', 'shop.html'));

    const products = adminData.products;
    // rendering pug file
    // path already mentioned in app.js
    // app.set('views', 'views')

    res.render('shop', {
        prods: products,
        docTitle: 'Shop',
        path: '/'
    });
});

module.exports = router;