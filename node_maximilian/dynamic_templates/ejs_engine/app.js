const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const adminData = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const app = express();


// using HTML templating dynamic templates
app.set('view engine', 'ejs');
// where to find '/views'
app.set('views', 'views');

app.use(bodyParser.urlencoded({
    exended: false
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminData.routes);
app.use(shopRoutes);

app.use((req, res, next) => {
    res.status(404);
    res.render('404', {
        pageTitle: 'Page Not Found',
        path: ""
    });
});


app.listen(3000);