const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const expressHbs = require('express-handlebars')

const adminData = require('./routes/admin');
const shopRoutes = require('./routes/shop');

const app = express();

// initialize handlebars engine, need to be import
// initialize with name hbs
app.engine('hbs', expressHbs({
    layoutsDir: 'views/layouts/',
    defaultLayout: 'main-layout',
    extname: 'hbs'
}));
// using HTML templating dynamic templates
app.set('view engine', 'hbs');
// where to find '/views'
app.set('views', 'views');

app.use(bodyParser.urlencoded({
    exended: false
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminData.routes);
app.use(shopRoutes);

app.use((req, res, next) => {
    res.status(404);
    res.render('404', {
        pageTitle: 'Page Not Found'
    })
});


app.listen(3000);