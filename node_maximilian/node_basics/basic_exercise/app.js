const http = require('http');
const fs = require('fs');

const requestHandler = (req, res) => {
    console.log(req.url, req.method, req.headers);

    const url = req.url;
    const method = req.method;

    if (url === '/') {
        res.write('<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Document</title></head>')
        res.write('<body><p>Greetings</p>')
        res.write('<form action="/create-user" method="POST"><label for="username">Create User</label><input type="text" id="username" name="username"><button type="submit">Submit</button></form>')
        res.write('</body>');
        res.write('</html>');
        return res.end();
    };

    if (url === '/users') {
        res.write('<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Document</title></head><body><ul><li>user 1</li><li>user 2</li><li>user 3</li></ul></body></html>');
        return res.end();
    };

    if (url === '/create-user' && method === 'POST') {
        const users = [];
        req.on('data', (chunks) => {
            console.log(chunks);
            users.push(chunks);
        });

        req.on('end', () => {
            const parsedBody = Buffer.concat(users).toString();
            console.log(parsedBody);
            res.statusCode = 302;
            res.setHeader('Location', '/');
            return res.end();
        });
    };
};

const server = http.createServer(requestHandler);

server.listen(3000);