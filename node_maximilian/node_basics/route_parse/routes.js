const fs = require('fs');

const requestHandler = (req, res) => {
    //console.log(req); // imprime todo o request
    console.log(req.url, req.method, req.headers);

    const url = req.url;
    const method = req.method;

    // ROUTING REQUEST '/' AND RESPONSE
    if (url === '/') {
        res.write('<html>');
        res.write('<head><title>Enter message</title></head>');
        res.write('<body><form action="/message" method="POST"><input type="text" name="message"><button type="submit">Send</button></form></body>');
        res.write('</html>');
        return res.end();
    };

    // ROUTING REQUEST '/message' AND RESPONSE
    if (url === '/message' && method === 'POST') {

        // PARSING REQUEST
        const req_body = [];
        req.on('data', (chunk) => {
            console.log(chunk);
            req_body.push(chunk);
        });

        // return, pois senao a callback demora responder e não da tempo de reroute para 'localhost:3000/', então executa a pagina inicial 'Hello world'
        return req.on('end', () => {
            console.log(req_body);
            const parsedBody = Buffer.concat(req_body).toString();
            console.log(parsedBody);
            const message = parsedBody.split('=')[1];
            console.log(message);
            // writeFileSync bloqueia execução da proxima linha até Sync ser executado
            // writeFileSync(file, data, callback)
            fs.writeFileSync('message.txt', message, err => {
                // somente sera executado se terminar de escrever message.txt
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end();
            });
        });
    };

    /* SEND RESPONSES TO OTHER THAN '/' OR '/MESSAGE'*/
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>My first page</title></head>');
    res.write('<body><h1>Hello from my Nodejs server</h1></body>');
    res.write('</html>');
    res.end(); //nodejs envia response para o cliente
    // process.exit();
};

/* OPTIONS TO EXPORT */

//module.exports = requestHandler;

// module.exports = {
//     handler: requestHandler,
//     someText: 'Some hard coded text from routes'
// };

// module.exports.handler = requestHandler;
// module.exports.someText = 'Some hard coded text from routes';

// NEW SHORTCUT
exports.handler = requestHandler;
exports.someText = 'Some hard coded text from routes';