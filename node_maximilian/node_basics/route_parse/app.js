// Core Modules: http, https, fs, path, os
// http => launch server, send requests
// https => launch ssl encoded server

// quando não usa relative ou absolute path
// procura por um modulo global http
// se tiver um arquivo http.js será ignorado
const http = require('http');

const routes = require('./routes')


console.log(routes.someText);

// será executado com um request (atualizar pagina do servidor)
const server = http.createServer(routes.handler);

// nodejs event loop, continua rodando enquanto exister "event listener" registrado

server.listen(3000);