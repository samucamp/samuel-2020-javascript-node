function getInteiroAleatorioEnter(min, max) {
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

let opcao = 0

while (opcao != -1) {
    opcao = getInteiroAleatorioEnter(-1, 10)
    console.log(`Opção escolhida foi ${opcao}.`)
}

console.log('Até a próxima!')

opcao = -1
// executa a primeira vez independente do valor de opcao
do {
    opcao = getInteiroAleatorioEnter(-1, 10)
    console.log(`Opção escolhida foi ${opcao}.`)
} while (opcao != -1)


console.log('Até a próxima 2!')