//INFLUENCIA DENTRO DE:
//SWITCH, LAÇO FOR E LAÇO WHILE
//continue funciona dentro de while e for
//NÃO INFLUENCIA NO BLOCO IF

const nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for (x in nums) {
    if (x == 5) break //break age em cima do bloco(for, while, switch) mais proximo
    console.log(x, nums[x])
}

for (y in nums) {
    if (y == 5) {
        continue //interrompe indice 5 e continua pras proximas
    }
    console.log(`${y} = ${nums[y]}`)
}

//RÓTULO
externo:
    for (a in nums) {
        for (b in nums) {
            if (a == 2 && b == 3) break externo //QUEBRA O RÓTULO INTEIRO, ignorando bloco atual
            console.log(`Par = ${a}, ${b}`)
        }
    }

console.log('Fim!')