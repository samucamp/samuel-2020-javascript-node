function soBoaNoticia(nota) {
    if (nota >= 7) {
        console.log('Aprovado com ' + nota)
    }
}

soBoaNoticia(8.1)
soBoaNoticia(5.1)

function seForVerdadeEuFalo(valor) {
    if (valor) {
        console.log('é verdade... ' + valor)
    }
}

seForVerdadeEuFalo()
seForVerdadeEuFalo(null)
seForVerdadeEuFalo(undefined)
seForVerdadeEuFalo(NaN)
seForVerdadeEuFalo('')
seForVerdadeEuFalo(0)
seForVerdadeEuFalo(-1)
seForVerdadeEuFalo(Infinity)
seForVerdadeEuFalo(' ')
seForVerdadeEuFalo([])
seForVerdadeEuFalo([1, 2])
seForVerdadeEuFalo({})

function teste1(num) {
    if (num > 7) console.log(num) //sem chaves executa 1a linha
    console.log('Final')
}

teste1(6)
teste1(8)

function teste2(num) {
    if (num > 7); //se colocar ';', termina a sentença
    {
        console.log(num) //sem chaves executa 1a linha
    }
    //console.log sempre vai executar, pois não tem filtro no bloco
}
//MORAL: EVITAR PONTO E VIRGULA EM BLOCOS CONDICIONAIS
teste2(6)
teste2(8)

const imprimirResultado = function (nota) {
    if (nota >= 7) {
        console.log('Aprovado!')
    } else {
        console.log('Reprovado!')
    }
}

imprimirResultado(10)
imprimirResultado(5)
imprimirResultado('Epa!') //cuidado, linguagem tipagem fraca
//será como reprovado
//utilizar ferramenta para testar se é um valor numérico



/****** ESTRUTURA COM ELSE IF  *******/
Number.prototype.entre = function (inicio, fim) {
    return this >= inicio && this <= fim
}

const ImprimirResultado = function (nota) {
    if (nota.entre(9, 10)) {
        console.log('Quadro de Honra')
    } else if (nota.entre(7, 8.99)) {
        console.log('Aprovado')
    } else if (nota.entre(4, 6.99)) {
        console.log('Recuperação')
    } else if (nota.entre(0, 3.99)) {
        console.log('Reprovado')
    } else {
        console.log('Nota Inválida')
    }
    console.log('Fim')
}

ImprimirResultado(10)
ImprimirResultado(8.9)
ImprimirResultado(6)
ImprimirResultado(2)
ImprimirResultado(-1)
ImprimirResultado(11)