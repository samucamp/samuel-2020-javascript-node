/* WHILE COM DETERMINADAS REPETIÇÕES */

let contador = 1

while (contador <= 10) {
    console.log(`O valora atual é: ${contador}.`)
    contador++
}

/* MUDANDO PARA FOR */

for (contador = 1; contador <= 10; contador++) {
    console.log(`O valora atual é: ${contador}.`)
}

/*outro exemplo de for com array*/

const notas = [6.7, 7.4, 9.8, 8.1, 7.7]
//verificando tamanho do array = array.lenth
for (let i = 0; i < notas.length; i++) {
    console.log(`notas = ${notas[i]}`)
}

//OBS.: DECLARADO I = 0 COM LET
//NÃO PERMITE UTILIZAR I FORA DO BLOCO
//MELHOR FORMA DE USAR.


/* UTILIZANDO FOR IN */
//FOR IN ELE RETORNA OS INDICES DO ARRAY
for (let i in notas) {
    console.log(i, notas[i])
}

const pessoa = {
    nome: 'Ana',
    sobrenome: 'Silva',
    idade: 29,
    peso: 64
}

for (let atributo in pessoa) {
    console.log(`${atributo} = ${pessoa[atributo]}`)
}