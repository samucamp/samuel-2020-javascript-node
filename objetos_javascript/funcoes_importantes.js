const pessoa = {
    nome: 'Rebeca',
    idade: 2,
    peso: 13
}

console.log(Object.keys(pessoa)) //retorna chaves array
console.log(Object.values(pessoa)) //retorna valores array
console.log(Object.entries(pessoa)) //retorna chave/valor array

//varredura em cada índice
Object.entries(pessoa).forEach(e => {
    console.log(`${e[0]}: ${e[1]}`)
})

//varredura em cada índice com destructuring
Object.entries(pessoa).forEach(([chave, valor]) => {
    console.log(`${chave}: ${valor}`)
})

//adiciona chave valor definido propriedade enumerable, writable
Object.defineProperty(pessoa, 'dataNascimento', {
    enumerable: false, //não consegue pegar a chave do elemento
    writable: false, //parecido com FREEZE, não pode editar
    value: '01/01/2019'
})

pessoa.dataNascimento = '01/01/2017'
console.log(pessoa.dataNascimento)
console.log(Object.keys(pessoa)) //não aparece dataNascimento
//pois foi definido enumerable como false...

//OBJECT.ASSIGN (ECMA SCRIPT 2015)
const dest = {
    a: 1
}
const o1 = {
    b: 2
}
const o2 = {
    c: 3,
    a: 4
}
const obj = Object.assign(dest, o1, o2)
//concatenação dos atributos dos 3 objetos, sobrescrevendo iguais
console.log(obj)

Object.freeze(obj)
obj.c = 12346
console.log(obj)