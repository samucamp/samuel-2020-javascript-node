//OBJETOS
//coleção dinâmica de pares chave: valor

//CONCEITOS IMPORTANTES
// - Abstração - interpretar/abstrair algo real para o código
// - Encapsulamento - algumas informações do objeto não relevantes para o usuário
// - Herança - herança de características do objeto "pai"
// - Polimorfismo - utilizar um genérico para gerar outros comuns

//CRIAÇÃO A PARTIR DO OBJECT EM JS
console.log(typeof Object, typeof new Object) //function, object
const produto = new Object
produto.nome = 'Cadeira'
produto['marca do produto'] = 'Generica' //evitar declarar assim
produto.preco = 220

console.log(produto)
//para deletar atributos
delete produto.preco
delete produto['marca do produto']
console.log(produto)

//CRIAÇÃO LITERAL - criação com {}
const carro = {
    modelo: 'A4',
    valor: 89000,
    proprietario: {
        nome: 'Raul',
        idade: 56,
        endereco: {
            logradouro: 'Rua ABC',
            numero: 123
        }
    },
    condutores: [{
        //array de objetos
        nome: 'Junior',
        idade: 19
    }, {
        nome: 'Ana',
        idade: 42
    }],
    calcularValorSeguro: function () {
        // ...
    }
}
console.log(carro)
carro.proprietario.endereco.numero = 1000
console.log(carro)
carro['proprietario']['endereco']['logradouro'] = 'Av Gigante'
console.log(carro)

delete carro.condutores //deleta o objeto e os subojt
console.log(carro)
delete carro.proprietario.endereco
delete carro.calcularValorSeguro
console.log(carro)
console.log(carro.condutores) //retorna undefined pois foi deletado
// console.log(carro.condutores.length) //da erro !!!

//CRIAÇÃO A PARTIR DE FUNÇÕES CONSTRUTORAS
//preco e desc estão encapsulados, escopo dentro da função
//nome é tornado externo utilizando "this"
function Produto(nome, preco, desc) {
    this.nome = nome
    this.getPrecoComDesconto = () => {
        return preco * (1 - desc)
    }
}

const p1 = new Produto('Caneta', 7.99, 0.15)
const p2 = new Produto('Notebook', 2998.99, 0.25)
console.log(p1.getPrecoComDesconto(), p2.getPrecoComDesconto())
console.log(p1.nome, p2.nome) //acessível

//FUNÇÃO FACTORY

function criarFuncionario(nome, salarioBase, faltas) {
    return {
        nome,
        salarioBase,
        faltas,
        getSalario() {
            return (salarioBase / 30) * (30 - faltas)
        }
    }
}

const f1 = criarFuncionario('Joao', 7980, 4)
const f2 = criarFuncionario('Maria', 11400, 1)
console.log(f1.getSalario(), f2.getSalario())

//OBJECT CREATE
const filha = Object.create(null)
filha.nome = 'Ana'
console.log(filha)

//FUNÇÃO FAMOSA QUE RETORNA OBJETO (JSON)
const fromJSON = JSON.parse('{"info": "Sou um JSON"}')
console.log(fromJSON.info)

//CONSTANTES OBJETOS
//Pq da pra trocar os valores do objeto, mesmo declarado CONST:?

const pessoa = {
    nome: 'Joao'
}
//pessoa -> mem address -> {objeto criado} conceito de PONTEIROS
pessoa.nome = 'Pedro'
//constante pessoa (REFERENCIA) não foi alterado
//continua apontando pro mesmo mem address
//foi alterado o dado (objeto) dentro do address
console.log(pessoa)
//o código abaixo gera problema, pois tenta apontar pra outro address
/* pessoa = { nome: 'Ana'} */

Object.freeze(pessoa)
//congela o objeto, tornando os dados constantes
pessoa.nome = 'Maria' //não pode alterar
pessoa.end = 'Rua' //não pode add
delete pessoa.nome //não pode deletar
console.log(pessoa.nome)
console.log(pessoa)

//CRIANDO COM FREEZE
const pessoaConstante = Object.freeze({
    nome: 'Joao'
})
console.log(pessoaConstante)

//MELHORIAS NOTAÇÃO LITERAL DE OBJETOS ECMA SCRIPT 2015
const a = 1
const b = 2
const c = 3

const obj1 = {
    a: a,
    b: b,
    c: c
}
const obj2 = {
    a,
    b,
    c
}
console.log(obj1, obj2)

const nomeAttr = 'nota'
const valorAttr = 7.87
const obj3 = {}
obj3[nomeAttr] = valorAttr
//pega o valor da variável pra criar o objeto
console.log(obj3)

const obj4 = {
    [nomeAttr]: valorAttr
}
console.log(obj4)

const obj5 = {
    funcao1: function () {
        /*...*/ }
}
const obj6 = {
    funcao2() {
        /*...*/ }
}

console.log(obj5, obj6)