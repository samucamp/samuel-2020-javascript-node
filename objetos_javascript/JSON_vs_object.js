//JAVA SCRIPT OBJECT NOTATION != OBJECT
//JSON formato de dados TEXTUAL
//usado para interoperabilidade entre sistemas
//não carrega nada especifico de cada sistema

const obj = {
    a: 1,
    b: 2,
    c: 3,
    soma() {
        return a + b + c
    }
}
console.log(obj)
console.log(JSON.stringify(obj))
//transforma em formato JSON
//gerou formato de dados, transformou em string, excluiu função

//FORMATOS INVÁLIDOS JSON
//console.log(JSON.parse("{ a: 1, b: 2, c: 3}"))
//console.log(JSON.parse("{ 'a':1, 'b':2, 'c':3 }"))
console.log(JSON.parse('{ "a":1, "b":2, "c":3 }'))
//transforma em objeto para JS
//Todos atributos deve ser aspas duplas
console.log(JSON.parse('{ "a":1, "b":"string", "c": true, "d":{}, "e": [] }'))

//OBSERVAÇÃO
//Utilizando aspas simples no texto geral
//Se precisar utilizar aspas simples dentro, precisa utilizar
//Barra para dar escapa e não confundir com fechamento
//Exemplo abaixo, mas para JSON qualquer string deve ser aspas duplas ""
/* console.log(JSON.parse('{ "a":1, "b": \'string\' }')) */