//Object.preventExtensions
//não permite que o OBJ seja extendido
//não consegue aumentar numero de atributos
//pode excluir atributos, diferente de freeze

const produto = Object.preventExtensions({
    nome: 'Qualquer',
    preco: 1.99,
    tag: 'promoção'
})

console.log(produto)

console.log('Extensível:', Object.isExtensible(produto))

produto.nome = 'Borracha' //permitido
produto.descricao = 'Borracha escolar branca' //não é possível add
delete produto.tag //permitido deletar
console.log(produto)

//Object.seal
//Possível: pode modificar valores de cada chave
//Não possível: não pode add nem del atributos

const pessoa = {
    nome: 'Juliana',
    idade: 35
}
Object.seal(pessoa)
console.log('Selado', Object.isSealed(pessoa))
pessoa.nome = 'Carol'
pessoa.sobrenome = 'Silva'
delete pessoa.nome
pessoa.idade = 29
console.log(pessoa)

//Object.freeze = selado, mas com valores constantes
//Já analisado em aulas passadas