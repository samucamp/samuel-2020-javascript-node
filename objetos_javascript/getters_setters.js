const sequencia = {
    _valor: 1, //convenção para programador só acessar interno
    get valor() {
        return this._valor++
    },
    set valor(valor) {
        if (valor > this._valor) {
            this._valor = valor
        }
    }
}

console.log(sequencia.valor, sequencia.valor) //get 2 vezes
sequencia.valor = 1000 //passa pelo set, torna valor = 1000
console.log(sequencia.valor, sequencia.valor) //get 2 vezes
sequencia.valor = 900 //passa pelo set, mas não atende condição
console.log(sequencia.valor, sequencia.valor)