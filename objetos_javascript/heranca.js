//Reutilizar código
//Herança de "objeto pai"
//Em JS só pode ter 1 herança

//HERANÇA 1 

const ferrari = {
    modelo: 'F40',
    velMax: 324
}

const volvo = {
    modelo: 'V40',
    velMax: 200
}

console.log(ferrari.prototype)
console.log(ferrari.__proto__)
console.log(ferrari.__proto__ === Object.prototype)
console.log(volvo.__proto__ === Object.prototype)
console.log(Object.prototype.__proto__) //NULL
console.log(Object.prototype.__proto__ === null)

function MeuObjeto() {}
console.log(typeof Object, typeof MeuObjeto)
console.log(Object.prototype, MeuObjeto.prototype)

// HERANÇA 2

Object.prototype.attr0 = '0' //não recomendado pois afeta todos obj
const avo = {
    attr1: 'A'
}
const pai = {
    __proto__: avo,
    attr2: 'B',
    attr3: '3'
}
//para referenciar herança, __proto__ : objeto pai
const filho = {
    __proto__: pai,
    attr3: 'C'
}
console.log(filho.attr)
console.log(filho.attr0)
console.log(filho.attr1)
console.log(filho.attr2)
console.log(filho.attr3) //nesse caso, já tem em filho,
//então não busca o attr3 do pai

const carro = {
    velAtual: 0,
    velMax: 200,
    acelerarMais(delta) {
        if (this.velAtual + delta <= this.velMax) {
            this.velAtual += delta
        } else {
            this.velAtual = this.velMax
        }
    },
    status() {
        return `${this.velAtual}Km/h de ${this.velMax}Km/h`
    }
}

const ferrarii = {
    modelo: 'F40',
    velMax: 324 //shadowing - vai sombrear o atributo original de carro
}


//this para referenciar objeto atual
//super para referenciar o prototipo
const volvoo = {
    modelo: 'V40',
    status() {
        return `${this.modelo}: ${super.status()}`
    }
}

//estabelecendo relação entre ferrari (filho), e carro (pai)
Object.setPrototypeOf(ferrarii, carro)
Object.setPrototypeOf(volvoo, carro)

console.log(ferrarii)
console.log(volvoo)

volvoo.acelerarMais(100)
console.log(volvoo.status())

ferrarii.acelerarMais(100)
console.log(ferrarii.status())


// HERANÇA 3

const pai1 = {
    nome: 'Pedro',
    corCabelo: 'preto'
}

//cria filha1 tendo como prototipo o objeto pai
const filha1 = Object.create(pai1)
filha1.nome = 'Ana'
console.log(filha1.corCabelo)

const filha2 = Object.create(pai1, {
    nome: {
        value: 'Bia',
        writable: false,
        enumerable: true
    }
})

console.log(filha2.nome)
filha2.nome = 'Carla' //não altera
console.log(`${filha2.nome} tem cabelo ${filha2.corCabelo}`)

console.log(Object.keys(filha1))
console.log(Object.keys(filha2))

for (let chave in filha2) {
    filha2.hasOwnProperty(chave)
        //saber se pertence ao proprio obj
        ?
        /*(true)*/
        console.log(chave) :
        /*(falso)*/
        console.log(`Por herança: ${chave}`)
}


//HERANCA 4

function MeuObjeto2() {}

console.log(MeuObjeto2.prototype)


const obj1 = new MeuObjeto2
const obj2 = new MeuObjeto2
console.log(obj1.__proto__ === obj2.__proto__)

console.log(MeuObjeto2.prototype === obj1.__proto__)

MeuObjeto2.prototype.nome = 'Anônimo'
MeuObjeto2.prototype.falar = function () {
    console.log(`Bom dia! Meu nome é ${this.nome}`)
}

obj1.falar()

obj2.nome = 'Rafael'
obj2.falar()

const obj3 = {}
obj3.__proto__ = MeuObjeto2.prototype
obj3.nome = 'Obj3'
obj3.falar()

//RESUMINDO...
console.log((new MeuObjeto2).__proto__ === MeuObjeto2.prototype)
console.log(MeuObjeto2.__proto__ === Function.prototype)
console.log(Function.prototype.__proto__ === Object.prototype)
console.log(Object.prototype.__proto__ === null) //existe atributo, não aponta pra nada
console.log(Object.prototype.__proto__ === undefined) //false pois existe atributo


// HERANÇA #05

console.log(typeof String) //function
console.log(typeof Array) //function
console.log(typeof Object) //function
//Todos eles tem um atributo chamado .prototype

String.prototype.reverse = function () {
    return this.split('').reverse().join('')
}

console.log('Escola Cod3r'.reverse())

Array.prototype.first = function () {
    return this[0]
}

console.log([1, 2, 3, 4, 5].first())
console.log(['a', 'b', 'c'].first())

//evitar alterar, pois altera globalmente
String.prototype.toString = function () {
    return 'Lascou tudo'
}

console.log(('Escola Cod3r').reverse()) //não vai funcionar direito


// HERANÇA #06

//função construtora
function Aula(nome, videoID) {
    this.nome = nome,
        this.videoID = videoID
}

const aula1 = new Aula('Bem vindo', 123)
const aula2 = new Aula('Até Breve', 456)
console.log(aula1, aula2)


//simulando operador new (que instancia)
//alterando os dados manualmente, inclusive prototype
//... = spread, rest, recebe concatenado parametros
function novo(f, ...params) {
    const obj = {}
    obj.__proto__ = f.prototype
    f.apply(obj, params) //executa função f passando obj e params
    return obj
}

const aula3 = novo(Aula, 'Bem vindo', 123)
const aula4 = novo(Aula, 'Até breve', 456)
console.log(aula3, aula4)