const axios = require('axios');

const stringfyRequest = (numerador, denominador) => {
    const numParsed = [];
    const denParsed = [];

    const numSplited = numerador.split(',');
    const denSplited = denominador.split(',');

    numSplited.forEach(i => {
        numParsed.push(parseInt(i))
    });

    denSplited.forEach(i => {
        denParsed.push(parseInt(i))
    });

    const data = JSON.stringify({
        numerador: numParsed,
        denominador: denParsed
    });

    console.log(numParsed);
    console.log(denParsed);

    return data;
};

module.exports = class Step {
    constructor(numerador, denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    };

    step = () => {
        const data = stringfyRequest(this.numerador, this.denominador);
        axios({
            method: 'get',
            url: 'http://localhost:5000/json',
            data: data
        }).then(res => console.log(res.data)).catch(err => console.log(err));
    };
};