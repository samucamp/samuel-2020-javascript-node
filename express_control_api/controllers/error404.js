const path = require('../utils/path')

exports.get404 = (req, res) => {
    res.sendFile(path + '/views/error404.html')
};