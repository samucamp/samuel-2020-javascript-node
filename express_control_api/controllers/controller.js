const path = require('../utils/path');

const Step = require('../models/stepResponse')

exports.getHome = (req, res) => {
    res.sendFile(path + '/views/home.html')
};

exports.getStep = (req, res) => {
    res.sendFile(path + '/views/step.html')
};

exports.postStep = (req, res) => {
    const numerador = req.body.numerador;
    const denominador = req.body.denominador;
    const test = new Step(numerador, denominador);
    test.step();
    res.redirect('/result')
};

exports.getResult = (req, res) => {
    res.sendFile(path + '/views/result.html')
};