const express = require('express');

const appController = require('./controllers/controller');

const router = express.Router();

// / => GET
router.get('/', appController.getHome);

// /simulacao => GET
router.get('/step', appController.getStep);

// /simulacao => POST
router.post('/step', appController.postStep);

// /result=> GET
router.get('/result', appController.getResult);

module.exports = router;