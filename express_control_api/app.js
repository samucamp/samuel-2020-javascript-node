const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');

const errorController = require('./controllers/error404')
const appRoutes = require('./router')

const {
    port,
    host
} = require('./utils/config')


const app = express()

// view engine and path
app.set('views', 'views');
// app.set('view engine', 'html');

// body parser forms urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

// serving static files
app.use(express.static(path.join(__dirname, 'public')));

// routes
app.use(appRoutes);

// middleware for error 404
app.use(errorController.get404);


app.listen(port, host, () => {
    console.log(`Server is running in ${host}:${port}`)
})