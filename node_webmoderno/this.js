// ENTENDENDO NUANCIAS DO THIS
console.log('Fora da função')
console.log(this === global)
console.log(this === module)
// this aponta para module.exports, não global

console.log(this === module.exports)
console.log(this === exports)
//apontam pra mesma referência

this.perigo = '...' //colocando valor na ref de module.exports
console.log(module.exports.perigo + 'module exports')

function logThis() {
    console.log('Dentro de uma função...')
    console.log(this === exports)
    console.log(this === module.exports)
    console.log(this === global)
    this.perigo = '...' //colocando dentro do escopo global
    console.log(global.perigo + 'escopo global')
}

logThis()
//dentro da função, não aponta para exports
//dentro de uma função, this aponta para global

