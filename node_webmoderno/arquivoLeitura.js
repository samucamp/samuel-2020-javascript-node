// referencia do file system
// modulo interno do node

const fs = require('fs')

const caminho = __dirname + '/arquivo.json'

// leitura de forma síncrona (Sync) caminho + encoding
// encoding do editor de texto VSCode = UTF-8
// se for um arquivo pesado, pode travar event loop
// não interessante pra trabalhar com IO
const conteudo = fs.readFileSync(caminho, 'utf-8')
console.log(conteudo)

// leitura assincrona
// precisa também de callback
// quando tiver carregado o arquivo, entra na callback
fs.readFile(caminho, 'utf-8', (err, conteudo) => {
    //poderia testar err, só tem acessso a conteudo com err=vazio
    //const config = conteudo não possível
    const config = JSON.parse(conteudo)
    console.log(`${config.db.host}:${config.db.port}`)
})

// outra forma de leitura, mais direta json
// deve incluir .json
const config = require('./arquivo.json')
console.log(config.db)
// repara a diferença de execução
// o ultimo console.log executa antes, devido ao tempo de leitura
// e espera da callback para executar o segundo console.log


// usando o file system para leitura de uma pasta
// __dirname = diretório atual
// retorna call back com erro e conteudo da pasta
// retorna conteudo dentro de array
fs.readdir(__dirname, (err, arquivos) => {
    console.log('Conteúdo da pasta...')
    console.log(arquivos)
})