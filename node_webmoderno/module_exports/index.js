const express = require('express');
const bodyParser = require('body-parser');

const saudacao = require('./saudacaoMid');
const usuarioApi = require('./api/usuario');

const app = express();

// outra forma de interagir com modulos
require('./api/produto')(app, 'com param!');

/*

produtoApi = require('./api/produto');
produtoApi(app, 'com param!');

*/

// app.post('/usuario', usuarioApi.salvar)
// app.get('/usuario', usuarioApi.obter)

app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(saudacao('Samuca'));

app.listen(3000, () => {
    console.log('My server is on');
});