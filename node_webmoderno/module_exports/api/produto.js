module.exports = (app, texto) => {
    function salvar(req, res) {
        res.send('Usuario => salvar ' + texto)
    }

    function obter(req, res) {
        res.send('Usuario => obter ' + texto)
    }

    //app recebido pode utilizar para post, get etc
    app.post('/usuario', salvar)
    app.get('/usuario', obter)

    return {
        salvar,
        obter
    }
}