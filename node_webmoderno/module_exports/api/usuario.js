function salvar(req, res) {
    res.send('Usuario => salvar')
}

function obter(req, res) {
    res.send('Usuario => obter')
}

// module.exports = {
//     salvar: salvar,
//     obter: obter
// }

// pode omitir chave valor pois são iguais
module.exports = {
    salvar,
    obter
}