const fs = require('fs')

const produto = {
    nome: 'Celular',
    preco: 1249.99,
    deconto: 0.15
}

fs.writeFile(__dirname + '/arquivoGerado.json', JSON.stringify(produto), err => {
    console.log(err || 'Arquivo salvo!')
    //caso de erro, recebe função de erro
    //caso esteja ok, printa arquivo salvo
})
// diretorio atual + nome do arquivo a ser gerado
// o que vai ser passado
// função caso de erro