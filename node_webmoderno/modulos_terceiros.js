// para buscar de lodash usa _, comum
const _ = require('lodash')
// busca index.js para ter acesso a todas funções

setInterval(() => console.log(_.random(500, 1000)), 500)

// Ctrl+Alt+N executa programa
// Ctrl+Alt+M da stop, mas não está funcionando
// Ctrl+Shift+P abre lista de comandos, clicar em CTRL ATL M

// utilizando nodemon no windows
// antes talvez deve habilitar execução de scripts no PowerShell
// Get-ExecutionPolicy 
// Set-ExecutionPolicy Unrestricted
// S
// Get-ExecutionPolicy

// instalando nodemon
// npm i -g nodemon
// nodemon
// cd node
// nodemon modulos_terceiros.js
// programa pode ser alterado durante execução, basta salvar
// nodemon da refresh no programa apos save (Ctrl+s)
// rs da restart
// CTRL+C para parar
