const contadorA = require('./instanciaUnica')
const contadorB = require('./instanciaUnica')
// retorna objeto

const contadorC = require('./instanciaNova')()
const contadorD = require('./instanciaNova')()
// retorna função, então add parentesis

contadorA.inc()
contadorA.inc()
// se o objeto já tiver sido criado, retorna mesma instancia
// node faz cache do objeto
// mudança no contadorA foi percebido no contadorB
console.log(contadorA.valor, contadorB.valor)

// utilizando função factory não tem esse problema
// para criar novas instancias
contadorC.inc()
contadorC.inc()
console.log(contadorC.valor, contadorD.valor)
