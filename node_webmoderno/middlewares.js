// MIDDLEWARE PATTERN (CHAIN OF RESPONSABILITY)
// contexto = objeto q vai conter os dados
// padrão web, no lugar de contexto, usa request e response
// proximo = next, função que será chamda quando necessário

const passo1 = (contexto, proximo) => {
    contexto.valor1 = 'mid1'
    proximo()
}

const passo2 = (contexto, proximo) => {
    contexto.valor2 = 'mid2'
    proximo()
}

const passo3 = contexto => contexto.valor3 = 'mid3'

// ...middlewares pode ser várias funcões
const exec = (contexto, ...middlewares) => {
    //indice do array de middlewares
    const execPasso = indice => {
        middlewares && indice < middlewares.length
            && middlewares[indice](contexto, () => execPasso(indice + 1))
    }
    execPasso(0) //disparo
}

const contexto = {}
exec(contexto, passo1, passo2, passo3)
//pode alterar a ordem e suprimir passos
//se executar o passo3 primeiro, ele não possui proximo()
console.log(contexto)