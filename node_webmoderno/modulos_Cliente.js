//forma de buscar dados de outros módulos
//somente para o que foi exportado
const moduloA = require('./moduloA_export')
const moduloB = require('./moduloB_export')
//não precisa coloca .js, é por padrão, mas pode

console.log(moduloA.ola)
console.log(moduloA.bemVindo)
console.log(moduloA.ateLogo)
console.log(moduloA)

console.log(moduloB.bomDia)
console.log(moduloB.boaNoite)
console.log(moduloB)