
//para objeto só é possível com module exports
//visível fora do módulo

module.exports = {
    bomDia: 'Bom dia',
    boaNoite() {
        return 'Boa noite'
    }
}
