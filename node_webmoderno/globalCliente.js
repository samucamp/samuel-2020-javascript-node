require('./global')

//ambos mesmo resultado
console.log(MinhaApp.saudacao())
//evitar usar global
console.log(global.MinhaApp.saudacao())

console.log(MinhaApp.nome)
MinhaApp.nome = 'Eita!'
console.log(MinhaApp.nome) //pode alterar
// opção: utilizar freeze em global.js = Object.freeze({})