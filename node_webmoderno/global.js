//global no browser = window

console.log(global)

global.MinhaApp = {
    saudacao() {
        return 'Estou globalmente'
    },
    nome: 'Sistema legal'
}

//qualquer outro arquivo terá acesso a MinhaApp