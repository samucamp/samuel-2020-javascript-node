const express = require('express');

const app = express();

const saudacao = require('./saudacaoMid');

// sem usar next() a requisicao para no middleware
app.use((req, res, next) => {
    console.log('Eu sempre serei chamado');
    console.log('\nSou um middleware');
    next();
});

app.use(saudacao('Samuca'));

// teste no postman
// http://localhost:3000/cliente/relatorio/?completo=true&ano=2018
app.get('/cliente/relatorio', (req, res, next) => {
    res.send(`Cliente realtório: completo ${req.query.completo} ano= ${req.query.ano}`);
})

app.post('/corpo', (req, res) => {
    let corpo = '';
    req.on('data', function (parte) {
        corpo += parte
    });

    req.on('end', function () {
        res.send(corpo);
    });
});

// :id representa uma url dinâmica
app.get('/cliente/:id', (req, res, next) => {
    res.send(`Cliente ${req.params.id} selecionado!`)
});

app.get('/json', (req, res) => {
    // res.json({
    //     name: 'iPad',
    //     price: '3200'
    // })

    res.json([{
            id: 'Ana',
            idade: 32
        },
        {
            id: 'Joao',
            idade: 49
        }
    ]);
});

// se mandar OPTIONS responde como POST
app.post('/opa', (req, res) => {
    res.send('OPA!!');
});

// se mandar /apenasget com POST, pula para app.use no final
app.get('/apenasget', (req, res) => {
    res.send('Só respondo get');
})

// como não tem url associada, qualquer requisicao bate nessa resposta.
// use atende qualquer requisicao (post, get)
// app.use((req, res, next) => {
//     res.send('Resposta!!');
// });

// app.all responde todas requisições

// callback executa quando o bind ser bem sucedido
// será executado primeiro
app.listen(3000, () => {
    console.log('My server is on');
});