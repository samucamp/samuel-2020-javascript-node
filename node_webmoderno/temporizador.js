// npm i node-schedule@1.3.0 -E
// formas de utilizar temporizadores

const schedule = require('node-schedule')

console.log(new Date().getDay())
console.log(new Date().getHours())

const tarefa1 = schedule.scheduleJob('*/5 * 23 * * 6', function () {
    console.log('Executando tarefa 1!', new Date().getSeconds())
})

setTimeout(function () {
    tarefa1.cancel()
    console.log('Cancelando tarefa 1!')
}, 20000)

// setImmediate
//setInterval

const regra = new schedule.RecurrenceRule()
// 0 - domingo, 6 - sabado
regra.dayOfWeek = [new schedule.Range(1, 6)]
regra.hour = 23
regra.second = 30

const tarefa2 = schedule.scheduleJob(regra, function () {
    console.log('Executando Tarefa 2!', new Date().getSeconds())
})
