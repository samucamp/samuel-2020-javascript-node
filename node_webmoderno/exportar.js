console.log(module.exports)
console.log(module.exports === this)
console.log(module.exports === exports)

this.a = 1 //utilizando o mesmo objeto module.exports
exports.b = 2 //utilizando o mesmo objeto module.exports
module.exports.c = 3

console.log(module.exports)

exports = null
//muda referencia de onde exports aponta

//mas module.exports continua com mesmos valores
console.log(module.exports)

exports = {
    nome: 'Teste'
}

console.log(module.exports)

// sempre que precisar atribuir novo objeto para ser exportado
// deve utilizar module.exports
module.exports = { publico: true }

console.log(module.exports)