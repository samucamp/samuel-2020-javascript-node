// acessar arquivo
// files.cod3r.com.br/curso-js/funcionarios.json
// arquivo no servidor da cod3r remoto

const url = 'http://files.cod3r.com.br/curso-js/funcionarios.json'
const axios = require('axios')

// axios é um client HTTP
// faz requisição pra obter info de algo remoto


const mulheres = f => f.genero === 'F'
const chineses = f => f.pais === 'China'
const menorSalario = (func, funcAtual) => {
    return func.salario < funcAtual.salario ? func : funcAtual
}

// método get requere do servidor o conteúdo do arquivo
axios.get(url).then(response => {
    const funcionarios = response.data
    console.log(funcionarios)


    //  filtrar china, mulher e menor salário
    const func = funcionarios
        .filter(mulheres)
        .filter(chineses)
        .reduce(menorSalario)

    console.log(func)
})


//node_modules pode ser excluido 
// pode ser instalado novamente com npm i
// pois já tem as informações no package json (axios dependency
// precisa estar na pasta funcionarios pois possui o package json


// com as alterações abaixo, é possível inicializar com npm start
// com dev não é possível npm dev, pois não é padrão
// precisa utilizar npm run dev

/* ALTERAÇÕES package.json

  "main": "funcionarios.js",
  "scripts": {
    "start": "nodemon",
    "dev": "nodemon",

*/
/*
O package.json é uma espécie de 
manifesto do seu projeto. Ele pode fazer várias coisas, 
completamente não relacionadas. Ele é um repositório 
central de configurações de ferramentas, por exemplo. 
Ele também é onde npm armazena os nomes e versões dos 
pacotes instalados.

name define o nome da aplicação ou pacote;
version indica a versão atual;
description é um resumo da sua aplicação/pacote;
main define o ponto de entrada da aplicação;
private (true) previne a sua aplicação de ser publicada
 acidentalmente no npm;
scripts define um conjunto de scripts Node para você executar;
dependencies define uma lista de pacotes npm instalados como 
dependências; devDependencies define uma lista de pacotes
npm instalados como dependências de desenvolvimento;
engines define quais versões de Node este pacote/aplicação
 funciona; browserslist é usado para dizer quais browsers 
 (e versões) você quer suportar;
*/