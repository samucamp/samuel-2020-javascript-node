# Samuel 2020 - JavaScript e NodeJS

Aprendendo a linguagem JavaScript e NodeJS de diversas fontes.

<br>

# Conteúdos

1. [About JavaScript](#javascript)
2. [About Node](#node)
3. [Node Package Manager (NPM)](#npm)
4. [NPM Scripts](#scripts)
5. [Referências](#ref)

<br>

# About JavaScript

<a name="javascript"></a>

- Weakly Typed Language – No explicit type assignment. Data types can be switched dynamically.

- Object-Oriented Language – Data can be organized in logical objects. [Primitive and reference types](http://academind.com/learn/javascript/reference-vs-primitive-values/).

- Versatile Language – Runs in browser & directly on a PC/server (with node). Can performed a broad variety of tasks.

<br>

# About Node

<a name="node"></a>

## Introduction

- Server-side platform built on V8 engine.
- Open source and cross platform.
- Capable of generating real time web apps.
- Event-drive, non-blocking I/O operation model.
- Efficient and light-weight.

## Instalando Node no UBUNTU

Mais informações sobre uso do Ubuntu em: [SAMUEL 2020 - Linux Ubuntu](https://gitlab.com/samucamp/samuel-2020-linux-ubuntu)

Talvez seja necessário instalar o curl

```bash
sudo apt install curl
```

Instalando Node

```bash
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Verificando versões

```bash
node -v
npm -v
```

Hello world

```javascript
console.log("hello world");
```

Para mais informações, verificar em [github node source](https://github.com/nodesource/distributions/blob/master/README.md)

## HTTP module

```javascript
const http = require("http");
```

## Common status code (http)

```
200 OK
300 Multiple Choices
301 Moved Permanently
302 Found
304 Not Modified
307 Temporary Redirect
400 Bad Request
401 Unauthorized
403 Forbidden
404 Not Found
410 Gone
500 Internal Server Error
501 Not Implemented
503 Service Unavailable
550 Permission denied
```

## MIME types (http)

```javascript
res.writeHead(200, { "Content-Type": "text/html" });
response.writeHead(200, { "Content-Type": "text/plain" });
res.writeHead(200, { "Content-Type": "application/json" });
res.writeHead(200, { "Content-Type": "application/pdf" });
res.writeHead(200, { "Content-Type": "audio/mp3" });
res.writeHead(200, { "Content-Type": "video/mp4" });
```

## File System

```javascript
// Reading file operation
fs.readFile();
fs.readFileSync();

// Write file operation
fs.writeFile();
fs.writeFileSync();

// Append file operation
fs.appendFile();
fs.appendFileSync();

// Rename file operation
fs.rename();
fs.renameSync();

//Delete (unlink) file operation
fs.unlink();
fs.unlinkSync();
```

## MVC (Model-View-Controller)

Separation of concerns

### Models

- Represents your data in your code
- Responsible for managin data (saving, fetching)
- Contains data-related logic

### Views

- What the users sees
- Decoupled from your application code
- Shouldn't contain too much logic (ex: handlebars)

### Controllers

- Routes
- Split across middleware Functions
- Connecting your models and your views
- Responsible to make sure thant model and view can communicate (in both directions)
- Contains the "in-between" logic

<br>

# Node Package Manager (NPM)

<a name="npm"></a>

- Nodejs package ecosystem.
- Node package manager.

O Node Package Manager é instalado junto com Node.

Verificando versões

```bash
npm -v
```

Informações rápidas dos possíveis comandos

```bash
npm
```

Criando package.json (utilizado para registrar informações do projeto, como nome, versão, desenvolvedor, dependências utilizadas. O arquivo pode ser reutilizado em qualquer máquina para instalar de forma rápidas todas dependências necessárias para o projeto). Para mais informações verificar em [docs.npmjs.com](https://docs.npmjs.com/files/package.json)

```bash
npm init -y
```

Será gerado um arquivo package.json. Para evitar publicação do pacote acidentalmente, adicione '"private": true' e remova 'main'.

```json
{
  "name": "my-module-name",
  "version": "1.0.0",
  "description": "Learning NPM",

  //DELETE "main": "index.js",
  "private": true //ADD

  "scripts": {
    "test": "echo \"Learning NPM\"",
  },
  "keywords": [],
  "author": "Samucamp",
  "license": "ISC"
}
```

Instalando pacotes. Especificando versão com @major.minor.patch

Será criado uma pasta **node_modules** com todos as dependencias.

```bash
npm install <package>
npm install <package>@<versioning>
npm install sass@1.26.3
```

Para instalar dependencias somente para desenvolvimento.

```bash
npm instal <package> --save-dev
npm install <package> -D
```

As dependências e suas versões são adicionadas ao package.json. Exemplo:

```json
{
  "name": "galeria-bootstrap",
  "version": "1.0.0",
  "description": "Projeto galeria com Bootstrap",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "Samucamp",
  "license": "ISC",
  "dependencies": {
    "<package>": "^major.minor.patch"
  },
  "devDependencies": {
    "sass": "^1.26.3"
  }
}
```

Para instalar as dependências listadas no package.json basta estar no diretório do arquivo e digitar o comando npm i ou npm install. Para ignorar as "devDependencies" deve adicionar a flag --production ou -D. Para ignorar as "dependencies" deve utilizar a flag --dev.

```bash
npm i
npm install
npm install --production
npm install --dev
```

As dependências são instaladas na pasta node_modules. A pasta node_modules deve ser apagada antes de enviar para o repositório remoto, ou até mesmo ignorada via .gitignore

package-lock.json também é gerado informando as versões de cada dependência utilizada (e dependências de dependências).

Mais informações sobre uso do NPM em: [Npmjs](https://docs.npmjs.com/)

<br>

# NPM Scripts

<a name="scripts"></a>

Quando o package.json é gerado, ele inicializa com um script test.

```json
{
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  }
}
```

Executando script test

```bash
npm test
```

Alguns scripts executam tarefas pre-determinadas se não forem adicionados ao package.json e customizados. Exemplo: start. Por padrão npm start executa o código "node server.js". Pode ser criado um arquivo server.js e adicionado código java script para subir um servidor.

```bash
npm start
```

Para utilizar scripts customizados (sem nome padrão) deve-se utilizar npm run <scripts>. Exemplo:

package.json file

```json
{
  "scripts": {
    "custom": "echo \"Custom script\"",
    "custom2": "node ./src/customScript.js"
  }
}
```

npm command

```bash
npm run custom
npm run custom2
```

É possível executar todos scripts de forma paralela ou serial, utilizando um pacote npm-run-all, e adicionando a flag --parallel ou --serial

package.json file

```json
  "scripts": {
    "custom": "echo \"Custom script\"",
    "custom2": "node ./src/customScript.js",
    "all": "npm-run-all --parallel custom custom2"
  }
  "devDependencies": {
    "npm-run-all": "^1.26.3"
  }
```

npm command

```bash
npm run all
```

<br>

# Referências

<a name="ref"></a>

> https://nodejs.dev/

> https://www.w3schools.com/jquery/jquery_ref_overview.asp

> https://www.w3schools.com/nodejs/default.asp

> https://www.w3schools.com/js/

> https://www.udemy.com/course/nodejs-the-complete-guide/

> https://www.udemy.com/course/curso-web/

> https://www.nodejsera.com/30-days-of-node.html
