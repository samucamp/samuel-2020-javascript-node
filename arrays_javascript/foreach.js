const aprovados = ['Agata', 'Aldo', 'Daniel', 'Raquel']

//Para cada elemento, chama function (callback)
//indice sempre é passado como 2o parâmetro
//então obrigatóriamente tem que ter o 1o parâmetro para ter o indice no 2o
aprovados.forEach(function (nome, indice, vetor, x) {
    console.log(`${indice+1}) ${nome}`)
    console.log(vetor)
    console.log(x)
})

//o terceiro parâmetro: o próprio array
//quarto parâmetro é undefined

aprovados.forEach(nome => console.log(nome))

const exibirAprovados = aprovado => console.log(aprovado)

aprovados.forEach(exibirAprovados)


//IMPLEMENTANDO O FOREACH
Array.prototype.forEach2 = function (callback) {
    for (let i = 0; i < this.length; i++) {
        callback(this[i], i, this)
    }
}

aprovados.forEach2(function (nome, indice) {
    console.log(`${indice+1}) ${nome}`)
})