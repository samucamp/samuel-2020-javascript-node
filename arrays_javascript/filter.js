//FORMA DE FILTRA UM ARRAY
//TAMBÉM SERVE PRA PERCORRER O ARRAY

const produtos = [{
        nome: 'Notebook',
        preco: 2499,
        fragil: false
    },
    {
        nome: 'iPad Pro',
        preco: 4199,
        fragil: true
    },
    {
        nome: 'Copo de Vidro',
        preco: 12.49,
        fragil: true
    },
    {
        nome: 'Copo de Plástico',
        preco: 18.99,
        fragil: false
    }
]

//apenas utilizando parâmetro dos valores: produtos
//pode usar parametros index e array também
console.log(produtos.filter(function (p) {

    //aplica lógica pra retornar true ou falso
    //true: mantem elemento
    //false: retira elemento
    //return false equivale a cada elemento não estar presente no array final
    //return true retorna todos elementos, sem filtrar nada

    return (p.preco > 2400)

}))

//A função gera um novo array, não mexendo no original
//produtos continua conforme criado
console.log(produtos)

const caro = produto => produto.preco >= 500
const fragil = produto => produto.fragil //já retorna boolean

console.log(produtos.filter(caro).filter(fragil))

// IMPLEMENTANDO O FILTER
// RESOLVER PROBLEMA

console.log(produtos)

Array.prototype.filter2 = function (callback) {
    const newArray = []
    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            newArray.push(this[i])
        }
    }
    return newArray
}

console.log(produtos.filter2(caro).filter2(fragil))