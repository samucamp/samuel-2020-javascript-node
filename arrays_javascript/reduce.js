const produtos = [{
        nome: 'Notebook',
        preco: 2499,
        fragil: false
    },
    {
        nome: 'iPad Pro',
        preco: 4199,
        fragil: true
    },
    {
        nome: 'Copo de Vidro',
        preco: 12.49,
        fragil: true
    },
    {
        nome: 'Copo de Plástico',
        preco: 18.99,
        fragil: false
    }
]

console.log(produtos.map(p => p.preco))

const resultado = produtos.map(p => p.preco).reduce(function (acumulador, atual) {
    console.log(acumulador, atual)
})

console.log('...' + resultado)

const resultado2 = produtos.map(p => p.preco).reduce(function (acumulador, atual) {
    console.log(acumulador, atual)
    return acumulador, atual
})

console.log('...' + resultado2)

const resultado3 = produtos.map(p => p.preco).reduce(function (acumulador, atual) {
    console.log(acumulador, atual)
    return acumulador + atual
})

console.log('...' + resultado3)

const resultado4 = produtos.map(p => p.preco).reduce(function (acumulador, atual) {
    console.log(acumulador, atual)
    return acumulador + atual
}, 0) //definindo valor inicial para acumulador

console.log('...' + resultado4)


//Todos ALUNOS SÃO BOLSITAS ?

const alunos = [{
        nome: 'Joao',
        nota: 7.3,
        bolsita: false
    },
    {
        nome: 'Maria',
        nota: 9.2,
        bolsita: true
    },
    {
        nome: 'Pedro',
        nota: 9.8,
        bolsita: false
    },
    {
        nome: 'Ana',
        nota: 8.7,
        bolsita: true
    }
]

const bolsistas = alunos.map(a => a.bolsita)
const todosBolsistas = (resultado, bolsista) => resultado && bolsita
console.log(bolsistas.reduce(todosBolsistas))

// ALGUM ALUNO É BOLSISTA ?

const algumBolsista = (resultado, bolsista) => resultado || bolsista
console.log(bolsistas.reduce(algumBolsista))

// IMPLEMENTAÇÃO DO REDUCE
// com valor inicial
Array.prototype.reduce2 = function (callback, valorInicial) {
    const inidiceInicial = valorInicial ? 0 : 1
    let acumulador = valorInicial || this[0]
    for (let i = inidiceInicial; i < this.length; i++) {
        acumulador = callback(acumulador, this[i], i, this)
    }
    return acumulador
}

const soma = (total, valor) => total + valor
const nums = [1, 2, 3, 4, 5, 6]
console.log(nums.reduce2(soma, 20))