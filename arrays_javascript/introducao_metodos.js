//ARRAY é um objeto especial.
//Não existe array como estrutura em JS
//Boa prática trabalhar com dados homogêneos dentro do array

console.log(typeof Array) //função array
console.log(typeof new Array) //instaciado objeto
console.log(typeof []) //array literal é objeto

let aprovados = new Array('Bia', 'Carlos', 'Ana')
console.log(aprovados)
console.log(aprovados[0], aprovados[1], aprovados[2])
aprovados = ['Bia', 'Carlos', 'Ana']
console.log(aprovados)
console.log(aprovados[0], aprovados[1], aprovados[2])

console.log(aprovados[3]) //undefined, não retorna erro

//Adicionando elemento, ou substituindo
aprovados[3] = 'Paulo'
console.log(aprovados)

//Método push mais apropriado para adicionar elementos
aprovados.push('Abia')
console.log(aprovados)
console.log(aprovados.length)

aprovados[9] = 'Rafael'
console.log(aprovados.length)
console.log(aprovados)
console.log(aprovados[5] === undefined)
console.log(aprovados[6] === undefined)
console.log(aprovados[7] === undefined)
console.log(aprovados[8] === undefined)

aprovados.sort() //ordena o array em ordem alfabética
console.log(aprovados)

//forma de excluir elemento de um array
//coloca undefined na posição, não alterando a ordem
delete aprovados[1]
console.log(aprovados)
console.log(aprovados[1])

aprovados = ['Bia', 'Carlos', 'Ana']
console.log(aprovados)
//função splice para remover, remover e adicionar, adicionar em indice
aprovados.splice(1 /*indice inicial*/ , 1 /*quantidade*/ )
console.log(aprovados)
//para adicionar, proximos parâmetros
aprovados.splice(1, 0, 'elemento1', 'elemento2')
console.log(aprovados)

/////////////
const pilotos = ['Vettel', 'Alonso', 'Raikkon', 'Massa']
//conteudo array pode ser alterado
//só não pode atribuir nada pro array const

pilotos.pop() //massa quebrou o carro
//.pop tira da ultima posição
console.log(pilotos)

pilotos.push('Verstappen')
console.log(pilotos)

pilotos.shift() //remove primeiro elemento
console.log(pilotos)

pilotos.unshift('Hamilton') //adiciona elemento na primeira posição
console.log(pilotos)

const algunsPilotos1 = pilotos.slice(2)
//novo array gerado, a partir do indice 2 inclusive
console.log(algunsPilotos1)

const algunsPilotos2 = pilotos.slice(1, 3)
//1 inclusive, 4 excluido, pega 1 2 
console.log(algunsPilotos2)


//SIMULANDO UM ARRAY COM UM OBJETO
const quaseArray = {
    0: 'Rafael',
    1: 'Ana',
    2: 'Bia'
}
console.log(quaseArray)
Object.defineProperty(quaseArray, 'toString', {
    value: function () {
        return Object.values(this)
    },
    enumerable: false
})

console.log(quaseArray[0])

const meuArray = ['Rafael', 'Ana', 'Bia']
console.log(quaseArray.toString(), meuArray)