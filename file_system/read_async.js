const fs = require('fs');

//file must be closed
fs.readFile(__dirname + '/data/message.txt', (err, data) => {
    if (err)
        throw err;

    console.log("Content :  " + data);
});

console.log("This will be logged first due to async \n");
console.log("Content will only be printed after readFile is done");