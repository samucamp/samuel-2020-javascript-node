const fs = require('fs');

content = 'Test message to be written'

fs.writeFile(__dirname + '/data/testeAsync.txt', content, (err) => {
    if (err) throw err;

    // if successful
    console.log('File already saved!');

});

console.log('This will be printed first!')