function falarDepoisDe(segundos, frase) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(frase)
            // reject(frase) vai para .catch
            // resolve só passa 1 parâmetros
            // se adicionar será ignorado
            // pra pasar mais itens, precisa usar objeto
        }, segundos * 1000)
    })
}

falarDepoisDe(3, 'Que legal!')
    .then(frase => frase.concat('?!?'))
    // then é função de promise
    // then é chamada depois de resolve
    .then(outraFrase => console.log(outraFrase))
    // then pode ser encadeado
    .catch(e => console.log(e))
// caso tenha erro (reject)
// catch é função de promise
// catch é chamada depois de reject