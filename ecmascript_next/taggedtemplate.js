// tagged template - processa o template dentro de uma função

function tag(partes, ...valores) {
    console.log(partes)
    console.log(valores)
    return 'Outra string'
}

const aluno = 'Gui'
const situacao = 'Aprovado'

// template string normal
console.log(`${aluno} está ${situacao}`)

// tagged template string utilizando tag
console.log(tag `${aluno} está ${situacao}.`)

// exemplo de aplicação

function real(partes, ...valores) {
    const resultado = []
    valores.forEach((valor, indice) => {
        valor = isNaN(valor) ? valor : `R$${valor.toFixed(2)}`
        resultado.push(partes[indice], valor)
    })
    return resultado.join('')
}

const preco = 29.99
const precoParcela = 11

//opção para renderizar a string

console.log(real `1x de ${preco} ou 3x de ${precoParcela}`)