/* ES 6 ----- ECMA SCRIPT 2015 */

// let e var
// let: escopo global, de função e de bloco
// var: escopo global e de função apenas
{
    var a = 2
    let b = 3
    console.log(b)
}

console.log(a)
// console.log(b) não é possível


// Template String
const produto = 'iPad'
console.log(`${produto} é caro!`)

// Operador destructuring
// operador rest ... 
const [l, e, ...tras] = 'Cod3r'
console.log(l)
console.log(e)
console.log(tras)

const [x, /*espaço vazio p ignorar segundo elemento*/ , y] = [1, 2, 3]
console.log(x, y)

//desestrutura idade e renomeia variavel i
const {
    idade: i,
    nome
} = {
    nome: 'Ana',
    idade: 9
}
console.log(i, nome)

// ARROW Function
// Funções arrow são funções anônimas
const soma = (a, b) => a + b //return implícito
// se precisar usar o corpo da função {}, deve usar return
console.log(soma(5, 2))

// se for função normal, this aponta pra global
// na função arrow, this aponta pra exports
const lexico1 = () => console.log(this === exports)
const lexico2 = lexico1.bind({}) //não é possível bind
lexico1()
lexico2()

// parametro default
function log(texto = 'Node') {
    console.log(texto)
}

log()
log('Parametro é mais forte')

// Operador rest (pode espalhar e pode agrupar)
function total(...numeros) {
    let total = 0
    numeros.forEach(n => total += n)
    return total
}
console.log(total(2, 3, 4, 5))

// Novos recursos relacionados aos OBJECTS

const obj = {
    a: 1,
    b: 2,
    c: 3
}
console.log(Object.values(obj))
console.log(Object.entries(obj))

// Melhorias notação literal de OBJECTS
const nome2 = 'Carla'
const pessoa = {
    nome2, //já cria nome: 'Carla'
    ola() {
        // equivale a ola: function(){}
        return 'Oi gente!'
    }
}
console.log(pessoa.nome2, pessoa.ola())
console.log(`${pessoa.ola()} meu nome é ${pessoa.nome2}`)

// Class - internamente vira uma função
// a partir de função construtora ou classes
// pode criar objetos usando 'new'
class Animal {}
class Cachorro extends Animal {
    falar() {
        return 'AU AU!'
    }
}

console.log(new Cachorro().falar())