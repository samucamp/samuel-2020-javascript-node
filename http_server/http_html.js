const http = require('http');

// to serve external html file system module will be required
const fs = require('fs');

const host = '127.0.0.1';
const port = 3000;

const filePath = './html/index.html'

const requestHandler = (req, res) => {
    res.statusCode = 200; // OK
    res.setHeader('Content-Type', 'text/html');

    fs.readFile(filePath, (err, data) => {
        if (err) throw err;

        // if no err then send response
        console.log('Operation success');
        res.end(data);
    });
};

const server = http.createServer(requestHandler);

server.listen(port, host, (err) => {
    if (err)
        return console.log(console.log('Error ocurred: ' + err));

    console.log('My server is listening on ' + host + ':' + port);
})

process.on('SIGTERM', () => {
    server.close(() => {
        console.log('My process terminated')
    });
});