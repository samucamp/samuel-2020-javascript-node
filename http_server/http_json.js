const http = require('http');

const host = '127.0.0.1';
const port = 3000;

const requestHandler = (req, res) => {
    res.statusCode = 200; // OK
    res.setHeader('Content-Type', 'application/json');

    let json_response = {
        status: 200,
        message: 'successful',
        result: ['teste1', 'teste2', 'teste3'],
        code: 1234
    };

    console.log('running');

    json_response = JSON.stringify(json_response);

    res.end(json_response);
}

const server = http.createServer(requestHandler);

server.listen(port, host, (err) => {
    if (err)
        return console.log(console.log('Error ocurred: ' + err));

    console.log('My server is listening on ' + host + ':' + port);
})

process.on('SIGTERM', () => {
    server.close(() => {
        console.log('My process terminated')
    });
});