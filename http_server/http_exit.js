const http = require('http');

const host = '127.0.0.1';
const port = 3000;

const requestHandler = (req, res) => {
    //res.writeHead(200, {"Content-Type": "text/plain"});
    res.statusCode = 200; // OK
    res.setHeader('Content-Type', 'text/plain');

    // content will be printed only after a request
    console.log('Hello world from my server');
    // serving string
    res.write('Serving this string for you\n');

    console.log(process.pid);

    res.end('\nMy server is working with success');
}

// req -> request
// res -> response
const server = http.createServer(requestHandler);

server.listen(port, host, (err) => {
    if (err)
        return console.log(console.log('Error ocurred: ' + err));

    console.log('My server is listening on ' + host + ':' + port);
})


// to activate this function and kill the process
// process.kill(process.pid, 'SIGTERM')
// this can be called from the same program or another nodejs process 
process.on('SIGTERM', () => {
    server.close(() => {
        console.log('My process terminated')
    });
});