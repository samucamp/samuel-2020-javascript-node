const http = require('http');
const fs = require('fs');

const requestHandler = (req, res) => {
    res.statusCode = 200; // OK
    res.setHeader('Content-Type', 'text/html');

    // logging request data
    console.log(req.url, req.method, req.headers);

    const url = req.url;
    const method = req.method;

    if (url === '/teste') {
        res.write('Just a test (page)')
        console.log('Just a test (console)')
        return res.end();
    };

    if (url === '/kill') {
        res.end(process.kill(process.pid, 'SIGTERM'));
    };

    if (url === '/') {

        fs.readFile('./html/index.html', (err, data) => {
            if (err) throw err;

            // if no err then send response
            console.log('Operation success');
            res.end(data);
        });
    } else return res.end('PAGE NOT FOUND');
}

const server = http.createServer(requestHandler).listen(3000);

process.on('SIGTERM', () => {
    server.close(() => {
        console.log('My process terminated')
    });
});