const http = require('http');

const host = '127.0.0.1';
const port = 3001;

// req -> request
// res -> response
const server = http.createServer((req, res) => {
    //res.writeHead(200, {"Content-Type": "text/plain"});
    res.statusCode = 200; // OK
    res.setHeader('Content-Type', 'text/plain');

    // content will be printed only after a request
    console.log('This process will kill http_exit');
    console.log('\n Please get the correct process.pid');
    // terminating another process
    process.kill(9634, 'SIGTERM');
    res.end('\nMy server is working with success');
});

server.listen(port, host, (err) => {
    if (err)
        return console.log(console.log('Error ocurred: ' + err));

    console.log('My server is listening on ' + host + ':' + port);
})