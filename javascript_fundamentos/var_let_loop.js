for (var i = 0; i < 10; i++) {
    console.log(i)
}

console.log('i= ', i)
//para variável var ok, pois não tem escopo de bloco

for (let j = 0; j < 10; j++) {
    console.log(j)
}

//console.log('j= ', j) 
//NÃO É POSSÍVEL, POIS LET TEM ESCOPO DE BLOCO

const funcs = [] //vetor vazio

for (var i = 0; i < 10; i++) {
    funcs.push(function () {
        console.log(i)
    })
}

funcs[2]()
funcs[8]()
//serão o mesmo valor utilizando var
const funcs2 = []

for (let j = 0; j < 10; j++) {
    funcs2.push(function () {
        console.log(j)
    })
}

funcs2[2]()
funcs2[8]()
//serão valores diferentes utilizando let (conceito de memória de bloco)