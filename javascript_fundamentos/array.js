//ARRAY É UMA ESTRUTURA LINEAR PARA AGRUPAR MULTIPLOS VALORES
//ACESSA OS ELEMENTOS A PARTIR DO ÍNDICE 0
//VETOR
//PODE COLOCAR QUALQUER COISA: STRING, NUMBER, ETC

const valores = [7.7, 8.9, 6.3, 9.2]
console.log(valores)
console.log(valores[0], valores[3])

console.log(valores[4]) //aceita, retorna undefined (false)
console.log(typeof valores[4]) //elemento undefined
console.log(!!valores[4])
valores[4] = 10
console.log(valores)

console.log(valores.length) //quantidades de indices usados

valores.push({
    id: 3
}, false, null, 'teste') //para adicionar novos valores
console.log(valores)
//O IDEAL É UTILIZAR O ARRAY DE FORMA HOMOGÊNEA PARA ORGANIZAR
//MAS O ARRAY NO JS É HETEROGENEO PERMITINDO QUALQUER TIPO

console.log(valores.pop()) //retorna e retira o ultimo elemento
console.log(valores)

delete valores[0] //deixa empty onde foi deletado
console.log(valores)

console.log(typeof valores) // EM JS UM ARRAY É TIPO OBJECT