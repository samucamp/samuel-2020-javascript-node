{
    {
        {
            {
                var sera = 'Será??'
                console.log(sera)
            }
        }
    }
}

console.log(sera)

function teste() {
    var local = 123
    console.log(local)
}
//console.log(local) não é possível pois local é variavel da funcao teste
teste()
//variável VAR dentro de blocos (não sendo função) é visível para todo programa
//variaveis fora de funções são variaveis globais (EVITAR)
//VAR ou é global ou é local, escopo de função

var numero = 1 //ignorado, sobescrito
{
    var numero = 2
    console.log('dentro = ', numero)
}
console.log('fora = ', numero)


//EXEMPLOS COM DECLARAÇÃO DE LET
//O LET É SEPARADO POR BLOCOS
let numero1 = 1
let numero2 = 3

{
    let numero1 = 2
    console.log('dentro =', numero1)
    console.log('dentro2 =', numero2) //procura no escopo mais abrangente
    // console.log('dentro2 =', numero3) não pode acessar antes de inicializar
}

let numero3 = 3
console.log('fora =', numero1)

//VAR: ESCOPO GLOBAL E DE FUNÇÃO
//LET: ESCOPO GLOBAL, DE FUNÇÃO E DE BLOCO

//CONCEITO DE HOISTING, OU IÇAMENTO

console.log('a =', a) //retorna UNDEFINED pois ocorreu o HOISTING
var a = 2
console.log('a =', a)

//SE NÃO HOUVER "VAR A" NO PROGRAMA, DA ERRO COM "NOT DEFINED" 

//DETRO DA FUNÇÃO TAMBÉM HÁ IÇAMENTO
function teste() {

    console.log('b = ', b)
    var b = 4
    console.log('b = ', b)
}
teste()
//console.log('b = 'b) //DA ERRO POR CAUSA DO ESCOPO

//HOISTING NÃO OCORRE UTILIZANDO LET