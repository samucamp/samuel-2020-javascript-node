//NULL E UNDEFINED CONCEITOS

let VALOR //não inicializada
console.log(VALOR) //UNDEFINED = não atribuido nada

VALOR = null //não aponta pra nenhum objeto na memória
console.log(VALOR)
//console.log(valor.toString()) //Erro !!
//CUIDADO: NÃO PODE ACESSAR NADA

const produto = {}
console.log(produto.preco) //undefined
console.log(produto) //vazio

produto.preco = 3.5
console.log(produto)

produto.preco = undefined //evite atribuir undefined
console.log(!!produto.preco)
console.log(produto)

produto.preco = null //sem preço
console.log(produto)
console.log(!!produto.preco)

//NULL E UNDEFINED SÃO FALSE
//DEIXAR O UNDEFINED PARA SER SETADO PELA LINGUAGEM, NÃO DEFINIR MANUALMENTE
//PADRÃO UTILIZAR NULL PARA "ZERAR" VARIÁVEL DE REFERÊNCIA (PONTEIRO)