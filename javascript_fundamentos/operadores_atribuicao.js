const a = 7 //SIMBOLO DE IGUAL EM CONTEXTO DE ATRIBUIÇÃO
let b = 3

b += a // b = b + a
console.log(b)
b -= 4 // b = b - 4
console.log(b)
b *= 2
console.log(b)
b /= 2
console.log(b)

//utilizado pra descobrir par ou ímpar
b %= 2
console.log(b) //resto da divisão b/2


//OPERADORES DESTRUCTURING
//NOVO RECURSO ES2015

const pessoa = {
    nome: 'Ana',
    idade: 5,
    endereco: {
        logradouro: 'Rua ABC',
        numero: 1000
    }
}
//operador de desestruturação
const {
    nome,
    idade
} = pessoa //tira de dentro do objeto pessoa
console.log(nome, idade)

const {
    nome: n,
    idade: i
} = pessoa
console.log(n, i)

const {
    sobrenome,
    bemHumorado
} = pessoa //UNDEFINED
console.log(sobrenome, bemHumorado) //retorna undefined


const {
    sobrenome2,
    bemHumorado2 = true
} = pessoa //UNDEFINED
console.log(sobrenome2, bemHumorado2) //retorna undefined

//acessando objeto interno a outro
const {
    endereco: {
        logradouro,
        numero,
        cep
    }
} = pessoa
console.log(logradouro, numero, cep) // cep = undefined

//DESTRUCTURING #2 ponto de vista de ARRAY

const [a2] = [10]
console.log(a2)

const [n1, , n3, , n5, n6 = 0] = [10, 7, 9, 8]
console.log(n1, n3, n5, n6)

const [ /*ignorado*/ , [ /*ignorado*/ , nota]] = [
    [, 8, 8],
    [9, 6, 8]
]
console.log(nota)

function rand({
    min = 0,
    max = 1000
}) {
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}
const obj = {
    max: 50,
    min: 40
}
console.log(rand(obj))
console.log(rand({}))
console.log(rand({
    min: 955
}))


//para contexto de array desestruturação
function rand2([min = 0, max = 1000]) {
    if (min > max)[min, max] = [max, min] //inversão
    const valor = Math.random() * (max - min) + min
    return Math.floor(valor)
}

console.log(rand2([50, 40]))
console.log(rand2([, 10]))
console.log(rand2([]))
//console.log(rand2()) //NÃO FUNCIONA, POIS TENTA DESESTRUTURAR ALGO NULO

/******* AULA 45 FUNDAMENTO ********/
//OPERADORES ARITMÉTICOS

const [ax, bx, cx, dx] = [3, 5, 1, 15]

//OPERADORES BINÁRIOS (2 OPERAÇÕES)
//EXISTEM UNÁRIOS E TERNÁRIOS TAMBÉM
const soma = ax + bx + cx + dx
const subtracao = dx - cx
const multiplicacao = ax * bx
const divisao = dx / ax
const modulo = ax % 2

console.log(soma, subtracao, multiplicacao, -divisao, modulo)


//OPERADORES RELACIONAIS TAMBÉM BINÁRIOS
//RESULTANDO SEMPRE É VERDADEIRO OU FALSO

console.log('01)', '1' == 1)
console.log('02)', '1' === 1) //extritamente igual
console.log('03)', '3' != 3)
console.log('04)', '3' !== 3) //extritamente diferente
console.log('05)', 3 < 2)
console.log('06)', 3 > 2)
console.log('07)', 3 <= 2)
console.log('08)', 3 >= 2)

const d1 = new Date(0)
const d2 = new Date(0)
console.log('09)', d1 === d2)
console.log('10)', d1 == d2)
console.log('11)', d1.getTime() === d2.getTime())
console.log('12)', d1.getTime() == d2.getTime())
console.log('13)', undefined == null)
console.log('14)', undefined === null)

//REGRA, UTILIZAR === PARA EVITAR IGUALAR DIFERENTES TIPOS


/****** OPERADORES LÓGICOS *******/

function compras(trabalho1, trabalho2) {
    const comprarSorvete = trabalho1 || trabalho2 //or
    /* trabalho1 | trabalho2 ou bit a bit */
    const comprarTv50 = trabalho1 && trabalho2 //and
    //const comprarTv32 = !!(trabalho1 ^ trabalho2) //bitwise xor
    const comprarTv32 = trabalho1 != trabalho2 //not simula xor
    const manterSaudavel = !comprarSorvete //operador not unário

    //para retornar varias variáveis, retorna como objeto
    //nesse caso não precisa de chave valor
    //pode omitir a chave (novo recurso 2015)
    return {
        comprarSorvete,
        comprarTv50,
        comprarTv32,
        manterSaudavel
    }
}

console.log(compras(true, true))
console.log(compras(true, false))
console.log(compras(false, true))
console.log(compras(false, false))

/***************OPERADORES UNÁRIOS **************/

let num1 = 1
let num2 = 2

num1++ //pos fixada
console.log(num1)
    --num1 //forma prefixada, forma prioritária em precedência
console.log(num1)

console.log(++num1 === num2--) //-- executa após comparação
console.log(num1 === num2) //aqui o -- já executou
//EVITAR ISSO EM CÓDIGO, POIS GERA CERTA CONFUSÃO


/*********** OPERADORES TERNÁRIOS ************/
//return e chaves ocultas por ser só uma linha
const resultado = nota => nota >= 7 ? 'Aprovado' : 'Reprovado'
console.log(resultado(7.1))
console.log(resultado(5.9))