console.log("Linha de Código")
//JavaScript organizado em sentenças e blocos de código
//Ponto e vírgula opcional para finalizar sentença
{
  {
    console.log("Olá");
    console.log("Mundo!") 
  }
}
console.log("teste")
1 + 3
for (X = 0; X <= 5; X++) {
  if (X >= 3) {
    console.log("condicao")
  }
}
/* var é o identificador */
var a = 3 //atribuição
let b = 1 //regra geral: evitar uso de var, geralmente usar let
/*var pode ser declarado novamente no mesmo bloco */
/*com let não pode declar a mesma variável de novo */
const c = 23
/*com a constante, não é possível atribuir outro valor*/
console.log(a, b, c)
console.log(a + b + c)
//possível realizar operações com variáveis declaradas de forma diferente

var t1 = 3
let t2 = 4
var t1 = 30
t2 = 40 //apenas possível atribuir outro valor
const t3 = 50 //não é possível variar
//t3 = 60, não possível para tipo const
console.log(t1, t3, t3)


/*
  Conceito de tipagem fraca em JavaScript
*/
let qualquer = 'Legal' //entre aspas simples texto livre
console.log(qualquer)
console.log(typeof qualquer)

qualquer = 3.654
console.log(qualquer)
console.log(typeof qualquer)

qualquer = false
console.log(qualquer)
console.log(typeof qualquer)

qualquer = 3.65443533434543353463634543534534543
console.log(qualquer)
console.log(typeof qualquer)

const peso1 = 1.1
const peso2 = Number('2.0')
console.log(peso1, peso2)
console.log(Number.isInteger(peso1))
console.log(Number.isInteger(peso2))

const avaliacao1 = 9.871
const avaliacao2 = 6.871
const total = avaliacao1 * peso1 + avaliacao2 * peso2
const media = total / (peso1 + peso2)
console.log(media)
console.log(media.toFixed(2))
console.log(media.toString(2)) //em binário
console.log(typeof media)
console.log(typeof Number) //Number= função, number= tipo de dado

console.log(7 / 0) //resultado Infinity

console.log("10" / 2) //tipagem fraca, ele converte automático

console.log("show" * 2) //resulta em NaN NOT A NUMBER

console.log(0.1 + 0.7) //há aproximação, resulta em 7.99999

/* Consegue chamar a função com literal em parêntesis */
console.log((10.5667).toFixed(2))
console.log((10.5667).toString(2))

//JAVA SCRIPT NÃO POSSUI OPERADOR DE EXPONENCIAL
//NECESSITA UTILIZAR A CLASSE MATH

const raio = 5.6
const area = Math.PI * Math.pow(raio, 2) //função potência
console.log(area)
console.log(Math)

console.log(Math.ceil(6.1)) //retorna arredondamento teto
console.log(Math.floor(6.1)) //arredonda pra baixo

console.log(Math.random()) //gera random (de quanto a quanto??)