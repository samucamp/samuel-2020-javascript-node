    const escola = "Cod3r"
    console.log(escola.charAt(4)) //retorna índice 4 (começa do 0)
    console.log(escola.charAt(5)) //não retorna nada
    console.log(escola.charCodeAt(3)) //retorna ASCII HTML
    console.log(escola.charCodeAt(4)) //retorna ASCII HTML
    console.log(escola.indexOf('3')) //retorna o índice relacionado ao digito
    console.log(escola.indexOf('C')) //retorna o índice relacionado ao digito    
    console.log(escola.substring(1)) //retorna a partir do índice 1
    console.log(escola.substring(1, 3)) //retonra do índice 1(incluso) até 3(não incluso)
    console.log(escola.replace(3, 'e'))

    console.log('Escola '.concat(escola.replace(3, 'e')).concat("!"))
    console.log('Escola ' + escola.replace(3, 'e') + "!") //string entende + como concatenar
    console.log('3' + 2) //string tem prioridade e reconhece + como concatenar
    console.log('10' / 2) //string não reconhece divisão, então converte 10 em number
    console.log(typeof ('10' / 2)) //number
    console.log(typeof ('3' + 2)) //string
    console.log('3' - 2) //string não reconhece menos, então converte em number
    console.log(typeof ('3' - 2)) //number
    console.log('Ana,Maria,Pedro'.split(',')) //separa como array

    //TEMPLATE STRINGS
    //backtick (crase)
    const nome = 'Rebeca'
    const concatenacao = 'Olá ' + nome + '!'
    console.log(nome, concatenacao)
    const template = ` 
        Olá
        ${nome}!
    `
    console.log(template)

    console.log(`1+1 = ${1+1}`) //${} para interpretar variáveis, função, aritmética

    //FUNÇÃO ARROW
    const up = texto => texto.toUpperCase()
    console.log(`Ei... ${up('cuidado')}`)