console.log(typeof Object)
class Produto {}
console.log(typeof Produto)
//CLASSE E OBJETOS SÃO FUNÇÃO EM JS
//FUNÇÃO É BLOCO DE CÓDIGO NOMEADO
//RECEBE (OU NÃO) PARÂMETROS E RETORNA (OU NÃO) VALORES

function imprimirSoma(a, b) {
    return a + b
}

console.log(imprimirSoma(3, 2))
console.log(imprimirSoma(3)) //se enviar 1 valor, b = undefined
//undefined + number = NaN
console.log(imprimirSoma(3, 1, 4, 5, 6, 7))
console.log(imprimirSoma()) //NaN
//aceita, mas pega apenas os 2 primeiros
console.log(typeof NaN)

function soma(a, b = 0) {
    console.log(a + b)
}

soma(3) //trata b=0
soma(3, 2) // recebe b=2

//É POSSÍVEL ARMAZENAR UMA FUNÇÃO EM VARIÁVEL
const printSoma = function (a, b) {
    console.log(a + b)
}

printSoma(7, 3)

//ARMAZENANDO FUNÇÃO ARROW EM UMA VARIÁVEL

const SOMA = (a, b) => {
    return a + b
}
console.log(SOMA(2, 9))

const subtracao = (a, b) => a - b //para apenas uma linha
console.log(subtracao(2, 3))

const imprimir2 = a => console.log(a) //para um parâmetro
imprimir2("Legal")

//CONCEITO DE NOTAÇÃO PONTO

function Obj(nome) {
    this.nome = nome //com THIS fica visível fora da funct
    this.exec = function () {
        console.log('Exec...')
    }
}

const obj2 = new Obj('Cadeira') //instanciando - transf em obj
const obj3 = new Obj('Mesa')
console.log(obj2.nome)
console.log(obj3.nome)
obj3.exec()