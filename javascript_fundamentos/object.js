//JSON = JAVA SCRIPT OBJECT NOTATION (FORMATO TEXTUAL)
//OBJECT EM JAVA SCRIPT É DIFERENTE !! (NOTAÇÃO LITERAL)
//É REPRESENTADO POR UM PARES DE CHAVES COM VALORES
const prod1 = {}
prod1.nome = 'Celular Ultra Mega'
prod1.preco = 4998.90
console.log(prod1)
prod1['desconto Legal'] = 0.40 //Evitar nomes com espaço
//pode ser atribuido com essa notação de cochete
console.log(prod1)

//outra forma de atribuir utilizando notação literal
const prod2 = {
    nome: 'Camisa Polo',
    preco: 79.90, //deve ser separado por vígula
    obj: {
        blabla: 1, //pode repetir o nome dentro de outro objeto
        obj: {
            blabla: 2
        }
    }
}

console.log(prod2)


// PARA OPERAÇÕES COM OBJETOS, É ATRIBUIDO O ENDEREÇO DE MEMÓRIA
// CONCEITO DE PONTEIROS
// SE ALTERAR PARA UMA VARIÁVEL APONTADA, AS OUTRAS TB ALTERA
let a = {}
a.nome = "Teste"
let b = {}
b.nome = "Opa"
console.log(a, b)

b = a
console.log(a, b)

b.nome = "Mudou"
console.log(a, b)

//FUNCTION VS OBJECT
//INSTANCIAR

console.log(typeof Object)
console.log(typeof new Object)

const Cliente = function () {}
console.log(typeof Cliente)
console.log(typeof new Cliente)

class Produto {}
console.log(typeof Produto)
console.log(typeof new Produto())

//Quando cria um objeto literalmente, já está instanciando

//CONCEITO PAR NOME E VALOR
const saudacao = 'Opa' //contexto LÉXICO 1, local físico de definição

function exec() {
    const saudacao = 'Falaaa' //contexto LÉXICO 2
    return saudacao
}

//objeto literal
const cliente = {
    nome: 'Pedro',
    idade: 32,
    peso: 90,
    endereco: {
        logradouro: 'Rua Muito Legal',
        numero: 123
    }

}

console.log(saudacao)
console.log(exec())
console.log(cliente)

//CONCEITO DE NOTAÇÃO PONTO
const obj1 = {}
obj1.nome = 'Bola' //notação ponto
console.log(obj1.nome)
obj1['nome'] = 'Bola2' //outra forma, acessando os atributos
console.log(obj1.nome)