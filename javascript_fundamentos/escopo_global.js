let a = 3

global.b = 123

this.c = 456
this.d = false
this.e = 'teste'

console.log(this.a) //let não define no escopo de global, mas é var global
console.log(global.b) //global equivale a WINDOW no browser
console.log(this.c)
//console.log(c)
console.log(module.exports === this)

console.log(module.exports)

//criando variavel sem VAR e sem LET
//Ela é colocada diretamente no objeto GLOBAL
//evitar declarar dessa forma
abc = 3
console.log(global.abc)

//ATUALIZADO ES6
/*export const e = 456
export const f = false
export const g = 'teste'*/
console.log(this)