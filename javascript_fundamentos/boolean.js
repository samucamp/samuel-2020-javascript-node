let isAtivo = false
console.log(isAtivo)

isAtivo = true
console.log(isAtivo)

//para checar true, negar duas vezes (!!)
isAtivo = 1
console.log(!!isAtivo)

console.log('os verdadeiros...')
console.log(!!3, !!-1, !!' ', !![], !!{}, !!Infinity, !!(isAtivo = true))

console.log('os falsos...')
console.log(!!(isAtivo = 0)) //retorna o zero como falso
console.log(!!null)
console.log(!!NaN)
console.log(!!0)
console.log(!!'')
console.log(!!undefined)

console.log('pra finalizar...')
console.log(!!('' || null || 0 || ' '))
console.log(('' || null || 0 || 'epa' || 123)) //retorna primeiro verdadeiro

let nome = 'Lucas'
console.log(nome || 'Desconhecido')