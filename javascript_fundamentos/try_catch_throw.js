function tratarErroELancar(erro) {
    throw new Error('...')
    //THROW É VALOR ENVIADO QUANDO DA ERRO
    //throw 10
    //throw true 
    //throw 'mensagem'
    /*throw {
        nome: erro.name,
        msg: erro.message,
        data: new Date
    }*/
}

function imprimirNomeGritado(obj) {
    try {
        //da erro pois está undefined, o correto é nome.
        console.log(obj.name.toUpperCase() + '!!!')
    } catch (e) {
        //se der erro no try,entra no catch
        tratarErroELancar(e)
    } finally {
        //é executado independente de ter erro ou não
        console.log('final')
    }
}

const obj = {
    nome: 'Roberto'
}

imprimirNomeGritado(obj)